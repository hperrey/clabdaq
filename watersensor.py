#!/usr/bin/env python
#
# Module for interacting with water sensors, for example the GRI 2505 5 Volt DC
# magnetic reed switch. Uses gpiozero.Button.when_[pressed/released] functions
# but allows to call multiple functions and keeps track of sensor/button names.
#

import gpiozero

class WaterSensor:
    """Class representing a single water sensor. Supported types are those that act
    as a switch when in contact with water, for example the GRI 2505 5 Volt DC
    magnetic reed switch.

    Besides the power inputs, the sensors have two pins that act as either a
    closed or open loop. Connect one end of the loop to a GPIO pin and the other
    to ground or 3.3V (see `pull_up` parameter below).

    :type name: str
    :param name:
    What the sensor should be referred to, e.g. a location.

    :type gpio: int
    :param gpio:
    The GPIO pin that the closed or open loop of the sensor is connected to.

    :type pull_up: bool
    :param pull_up:
    If :data:`True` (the default), the GPIO pin will be pulled high by
        default.  In this case, connect the other side of the button to ground.
        If :data:`False`, the GPIO pin will be pulled low by default. In this
        case, connect the other side of the button to 3V3.

    """
    def __init__(self, name, gpio, pull_up=True):
        self.name = name
        self.button = gpiozero.Button(gpio, pull_up=pull_up)

class WaterAlarmPanel:
    """Class connecting several :class:`WaterSensor` and monitoring their status. An
    event will be triggered should the sensor change state. Several functions
    that will be called in such an event can be set up using `whenOpened` and
    `whenClosed` methods.

    """
    def __init__(self):
        self.sensors = []
        self.alertsClose = []
        self.alertsOpen = []

    def addSensor(self, name, gpio, pull_up=True):
        """
        Add a new sensor to be monitored.

        Parameters: see :class:`WaterSensor`
        """
        s = WaterSensor(name, gpio, pull_up)
        self.sensors.append(s)
        s.button.when_released = self.__alertOpen__
        s.button.when_pressed = self.__alertClose__

    def allClosed(self):
        """returns 'True' if all sensors are in the closed
        state.

        """
        for sens in self.sensors:
            if not self.isSensorClosed(sens):
                return False
        return True

    def allOpen(self):
        """returns 'True' if all sensors are in the open
        state.

        """
        for sens in self.sensors:
            if not self.isSensorOpen(sens):
                return False
        return True

    def isSensorClosed(self, s):
        """returns 'True' if a given sensor is in the closed
        state.
        """
        return s.button.is_pressed

    def isSensorOpen(self, s):
        """returns 'True' if a given sensor is in the open
        state.
        """
        return not s.button.is_pressed

    def isClosed(self, name):
        """returns 'True' if a given sensor represented by its `name` is in the closed
        state. Throws 'KeyError' if sensor name was not found.

        """
        for sens in self.sensors:
            if name == sens.name:
                return self.isSensorClosed(sens)
        raise KeyError(f"Sensor {name} not found")

    def isOpen(self, name):
        """returns 'True' if a given sensor represented by its `name` is in the open
        state. Throws 'KeyError' if sensor name was not found.

        """
        return not self.isSensorClosed(name)

    def getState(self, name):
        """returns string "open" if a given sensor represented by its `name` is in the open
        state, "closed" otherwise. Throws 'KeyError' if sensor name was not found.

        """
        if self.isSensorClosed(name):
            return "closed"
        else:
            return "open"

    def getSensorName(self, b):
        """ returns the name of a given sensor represented by the gpiozero.Button `b` """
        for sens in self.sensors:
            if b == sens.button:
                return sens.name
        return None

    def whenOpened(self, fcn):
        """Adds a function to be called when any of the sensors changes state from
        closed to open. The function will be called with the sensor's name as
        argument.

        """
        self.alertsOpen.append(fcn)

    def whenClosed(self, fcn):
        """Adds a function to be called when any of the sensors changes state from open
        to closed. The function will be called with the sensor's name as
        argument.

        """
        self.alertsClose.append(fcn)

    def softTrigger(self):
        """Checks current state and calls the functions usually reserved for state
        changes. Useful to react to the current state after initialization.

        """
        for sens in self.sensors:
            if sens.button.is_pressed:
                self.__alertClose__(sens.button)
            else:
                self.__alertOpen__(sens.button)

    def __alertOpen__(self, s):
        """ Method is triggered when circuit changes to open state and calls functions previously added by calling whenOpened() """
        for fcn in self.alertsOpen:
            fcn(self.getSensorName(s))

    def __alertClose__(self, s):
        """ Method is triggered when circuit changes to closed state and calls functions previously added by calling whenClosed() """
        for fcn in self.alertsClose:
            fcn(self.getSensorName(s))


def main():
    import time
    def myAlertOpen(sn):
        print(f"changed state to open for sensor {sn}")

    def myAlertClosed(sn):
        print(f"changed state to closed for sensor {sn}")

    p = WaterAlarmPanel()
    p.addSensor("daqbox", 22)
    p.addSensor("detector box", 23)

    p.whenOpened(myAlertOpen)
    p.whenClosed(myAlertClosed)

    # imitate button press
    #p.__alertOpen__(p.sensors[0].button)
    #p.__alertClose__(p.sensors[1].button)

    while True:
        print("sleeping")
        time.sleep(1)

if __name__ == '__main__':
    main()
