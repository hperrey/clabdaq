# printinfo.py
#
# This file is part of the LibTiePie programming examples.
#
# Find more information on http://www.tiepie.com/LibTiePie .

from __future__ import print_function
from libtiepie import *
from libtiepie.exceptions import LibTiePieException
from libtiepie.device import Device
from libtiepie.oscilloscope import Oscilloscope
from libtiepie.generator import Generator
from libtiepie.i2chost import I2CHost
from libtiepie.server import Server

import logging

def print_library_info(log):
    log.info('Library:')
    log.info('  Version      : ' + str(library.version) + library.version_extra)
    log.info('  Configuration: ' + library.config_str)


def print_device_info(dev, log, full=True):
    if not isinstance(dev, Device):
        raise

    log.info('Device:')
    log.info('  Name                      : ' + dev.name)
    log.info('  Short name                : ' + dev.name_short)
    log.info('  Serial number             : ' + str(dev.serial_number))
    try:
        log.info('  Calibration date          : {0:%Y-%m-%d}'.format(dev.calibration_date))
    except LibTiePieException:
        pass
    log.info('  Product id                : ' + str(dev.product_id))
    log.info('  Vendor id                 : ' + str(dev.vendor_id))
    try:
        log.info('  Driver version            : ' + str(dev.driver_version))
    except LibTiePieException:
        pass
    try:
        log.info('  Firmware version          : ' + str(dev.firmware_version))
    except LibTiePieException:
        pass
    try:
        log.info('  IPv4 address              : ' + ipv4_str(dev.ipv4_address))
    except LibTiePieException:
        pass
    try:
        log.info('  IP port                   : ' + str(dev.ip_port))
    except LibTiePieException:
        pass

    log.info('  Has battery               : ' + str(dev.has_battery))
    if dev.has_battery:
        log.info('  Battery:')
        try:
            log.info('    Charge                  : ' + str(dev.battery_charge) + ' %')
        except LibTiePieException:
            pass
        try:
            log.info('    Time to empty           : ' + str(dev.battery_time_to_empty) + ' minutes')
        except LibTiePieException:
            pass
        try:
            log.info('    Time to full            : ' + str(dev.battery_time_to_full) + ' minutes')
        except LibTiePieException:
            pass
        try:
            log.info('    Charger connected       : ' + str(dev.is_battery_charger_connected))
        except LibTiePieException:
            pass
        try:
            log.info('    Charging                : ' + str(dev.is_battery_charging))
        except LibTiePieException:
            pass
        try:
            log.info('    Broken                  : ' + str(dev.is_battery_broken))
        except LibTiePieException:
            pass

    if full:
        if isinstance(dev, Oscilloscope):
            print_oscilloscope_info(dev, log)
        elif isinstance(dev, Generator):
            print_generator_info(dev, log)
        elif isinstance(dev, I2CHost):
            print_i2c_info(dev, log)


def print_oscilloscope_info(scp, log):
    if not isinstance(scp, Oscilloscope):
        raise

    log.info('Oscilloscope:')
    log.info('  Channel count             : ' + str(len(scp.channels)))
    log.info('  Connection test           : ' + str(scp.has_connection_test))
    log.info('  Measure modes             : ' + measure_mode_str(scp.measure_modes))
    log.info('  Measure mode              : ' + measure_mode_str(scp.measure_mode))
    log.info('  Auto resolution modes     : ' + auto_resolution_mode_str(scp.auto_resolution_modes))
    log.info('  Auto resolution mode      : ' + auto_resolution_mode_str(scp.auto_resolution_mode))
    log.info('  Resolutions               : ' + ', '.join(map(str, scp.resolutions)))
    log.info('  Resolution                : ' + str(scp.resolution))
    log.info('  Resolution enhanced       : ' + str(scp.is_resolution_enhanced))
    log.info('  Clock outputs             : ' + clock_output_str(scp.clock_outputs))
    log.info('  Clock output              : ' + clock_output_str(scp.clock_output))
    try:
        log.info('  Clock output frequecies   : ' + ', '.join(map(str, scp.clock_output_frequencies)))
        log.info('  Clock output frequency    : ' + str(scp.clock_output_frequency))
    except LibTiePieException:
        pass
    log.info('  Clock sources             : ' + clock_source_str(scp.clock_sources))
    log.info('  Clock source              : ' + clock_source_str(scp.clock_source))
    try:
        log.info('  Clock source frequecies   : ' + ', '.join(map(str, scp.clock_source_frequencies)))
        log.info('  Clock source frequency    : ' + str(scp.clock_source_frequency))
    except LibTiePieException:
        pass

    log.info('  Record length max         : ' + str(scp.record_length_max))
    log.info('  Record length             : ' + str(scp.record_length))
    log.info('  Sample frequency max      : ' + str(scp.sample_frequency_max))
    log.info('  Sample frequency          : ' + str(scp.sample_frequency))

    if scp.measure_mode == MM_BLOCK:
        log.info('  Segment count max         : ' + str(scp.segment_count_max))
        if scp.segment_count_max > 1:
            log.info('  Segment count             : ' + str(scp.segment_count))

    if scp.has_trigger:
        log.info('  Pre sample ratio          : ' + str(scp.pre_sample_ratio))
        to = scp.trigger_time_out
        if to == TO_INFINITY:
            to = 'Infinite'
        log.info('  Trigger time out          : ' + str(to))
        if scp.has_trigger_delay:
            log.info('  Trigger delay max         : ' + str(scp.trigger_delay_max))
            log.info('  Trigger delay             : ' + str(scp.trigger_delay))
        if scp.has_trigger_hold_off:
            log.info('  Trigger holf off count max: ' + str(scp.trigger_hold_off_count_max))
            log.info('  Trigger holf off count    : ' + str(scp.trigger_hold_off_count))

    if len(scp.channels) > 0:
        num = 0
        for ch in scp.channels:
            log.info('  Channel {:d}:'.format(num))
            log.info('    Connector type          : ' + connector_type_str(ch.connector_type))
            log.info('    Differential            : ' + str(ch.is_differential))
            log.info('    Impedance               : ' + str(ch.impedance))
            log.info('    Connection test         : ' + str(ch.has_connection_test))
            log.info('    Available               : ' + str(ch.is_available))
            log.info('    Enabled                 : ' + str(ch.enabled))
            log.info('    Bandwidths              : ' + ', '.join(map(str, ch.bandwidths)))
            log.info('    Bandwidth               : ' + str(ch.bandwidth))
            log.info('    Couplings               : ' + coupling_str(ch.couplings))
            log.info('    Coupling                : ' + coupling_str(ch.coupling))
            log.info('    Auto ranging            : ' + str(ch.auto_ranging))
            log.info('    Ranges                  : ' + ', '.join(map(str, ch.ranges)))
            log.info('    Range                   : ' + str(ch.range))
            log.info('    Probe gain              : ' + str(ch.probe_gain))
            log.info('    Probe offset            : ' + str(ch.probe_offset))
            log.info('    Has Safe Ground         : ' + str(ch.has_safe_ground))
            if ch.has_safe_ground:
                log.info('    Safe Ground Enabled     : ' + str(ch.safe_ground_enabled))
                log.info('    Safe Ground Threshold   : ' + str(ch.safe_ground_threshold))
                log.info('    Safe Ground Thresh. Min : ' + str(ch.safe_ground_threshold_min))
                log.info('    Safe Ground Thresh. Max : ' + str(ch.safe_ground_threshold_max))
            if ch.has_trigger:
                tr = ch.trigger
                log.info('    Trigger:')
                log.info('      Available             : ' + str(tr.is_available))
                log.info('      Enabled               : ' + str(tr.enabled))
                log.info('      Kinds                 : ' + trigger_kind_str(tr.kinds))
                log.info('      Kind                  : ' + trigger_kind_str(tr.kind))
                log.info('      Level modes           : ' + trigger_level_mode_str(tr.level_modes))
                log.info('      Level mode            : ' + trigger_level_mode_str(tr.level_mode))
                log.info('      Levels                : ' + ', '.join(map(str, tr.levels)))
                log.info('      Hystereses            : ' + ', '.join(map(str, tr.hystereses)))
                log.info('      Conditions            : ' + trigger_condition_str(tr.conditions))
                if tr.conditions != TCM_NONE:
                    log.info('      Condition             : ' + trigger_condition_str(tr.condition))
                log.info('      Times                 : ' + ', '.join(map(str, tr.times)))
            num += 1

    print_trigger_inputs_info(scp, log)
    print_trigger_outputs_info(scp, log)


def print_generator_info(gen, log):
    if not isinstance(gen, Generator):
        raise

    log.info('Generator:')
    log.info('  Connector type            : ' + connector_type_str(gen.connector_type))
    log.info('  Differential              : ' + str(gen.is_differential))
    log.info('  Controllable              : ' + str(gen.is_controllable))
    log.info('  Impedance                 : ' + str(gen.impedance))
    log.info('  Resolution                : ' + str(gen.resolution))
    log.info('  Output value min          : ' + str(gen.output_value_min))
    log.info('  Output value max          : ' + str(gen.output_value_max))
    log.info('  Output on                 : ' + str(gen.output_on))
    if gen.has_output_invert:
        log.info('  Output invert             : ' + str(gen.output_invert))

    log.info('  Modes native              : ' + generator_mode_str(gen.modes_native))
    log.info('  Modes                     : ' + generator_mode_str(gen.modes))
    if gen.modes != GMM_NONE:
        log.info('  Mode                      : ' + generator_mode_str(gen.mode))
        if (gen.mode & GMM_BURST_COUNT) != 0:
            log.info('  Burst active              : ' + str(gen.is_burst_active))
            log.info('  Burst count max           : ' + str(gen.burst_count_max))
            log.info('  Burst count               : ' + str(gen.burst_count))
        if (gen.mode & GMM_BURST_SAMPLE_COUNT) != 0:
            log.info('  Burst sample count max    : ' + str(gen.burst_sample_count_max))
            log.info('  Burst sample count        : ' + str(gen.burst_sample_count))
        if (gen.mode & GMM_BURST_SEGMENT_COUNT) != 0:
            log.info('  Burst segment count max   : ' + str(gen.burst_segment_count_max))
            log.info('  Burst segment count       : ' + str(gen.burst_segment_count))

    log.info('  Signal types              : ' + signal_type_str(gen.signal_types))
    log.info('  Signal type               : ' + signal_type_str(gen.signal_type))

    if gen.has_amplitude:
        log.info('  Amplitude min             : ' + str(gen.amplitude_min))
        log.info('  Amplitude max             : ' + str(gen.amplitude_max))
        log.info('  Amplitude                 : ' + str(gen.amplitude))
        log.info('  Amplitude ranges          : ' + ', '.join(map(str, gen.amplitude_ranges)))
        log.info('  Amplitude range           : ' + str(gen.amplitude_range))
        log.info('  Amplitude auto ranging    : ' + str(gen.amplitude_auto_ranging))

    if gen.has_frequency:
        log.info('  Frequency modes           : ' + frequency_mode_str(gen.frequency_modes))
        log.info('  Frequency mode            : ' + frequency_mode_str(gen.frequency_mode))
        log.info('  Frequency min             : ' + str(gen.frequency_min))
        log.info('  Frequency max             : ' + str(gen.frequency_max))
        log.info('  Frequency                 : ' + str(gen.frequency))

    if gen.has_offset:
        log.info('  Offset min                : ' + str(gen.offset_min))
        log.info('  Offset max                : ' + str(gen.offset_max))
        log.info('  Offset                    : ' + str(gen.offset))

    if gen.has_phase:
        log.info('  Phase min                 : ' + str(gen.phase_min))
        log.info('  Phase max                 : ' + str(gen.phase_max))
        log.info('  Phase                     : ' + str(gen.phase))

    if gen.has_symmetry:
        log.info('  Symmetry min              : ' + str(gen.symmetry_min))
        log.info('  Symmetry max              : ' + str(gen.symmetry_max))
        log.info('  Symmetry                  : ' + str(gen.symmetry))

    if gen.has_width:
        log.info('  Width min                 : ' + str(gen.width_min))
        log.info('  Width max                 : ' + str(gen.width_max))
        log.info('  Width                     : ' + str(gen.width))

    if gen.has_edge_time:
        log.info('  Leading edge time min     : ' + str(gen.leading_edge_time_min))
        log.info('  Leading edge time max     : ' + str(gen.leading_edge_time_max))
        log.info('  Leading edge time         : ' + str(gen.leading_edge_time))
        log.info('  Trailing edge time min    : ' + str(gen.trailing_edge_time_min))
        log.info('  Trailing edge time max    : ' + str(gen.trailing_edge_time_max))
        log.info('  Trailing edge time        : ' + str(gen.trailing_edge_time))

    if gen.has_data:
        log.info('  Data length min           : ' + str(gen.data_length_min))
        log.info('  Data length max           : ' + str(gen.data_length_max))
        log.info('  Data length               : ' + str(gen.data_length))

    print_trigger_inputs_info(gen, log)
    print_trigger_outputs_info(gen, log)


def print_i2c_info(i2c, log):
    if not isinstance(i2c, I2CHost):
        raise

    log.info('I2C Host:')
    log.info('  Internal addresses        : ' + ', '.join(map(str, i2c.internal_addresses)))
    log.info('  Speed max                 : ' + str(i2c.speed_max))
    log.info('  Speed                     : ' + str(i2c.speed))

    print_trigger_inputs_info(i2c, log)
    print_trigger_outputs_info(i2c, log)


def print_trigger_inputs_info(dev, log):
    if not isinstance(dev, Device):
        raise

    if len(dev.trigger_inputs) > 0:
        num = 1
        for trin in dev.trigger_inputs:
            log.info('  Trigger input {:d}:'.format(num))
            log.info('    Id                      : ' + str(trin.id))
            log.info('    Name                    : ' + str(trin.name))
            log.info('    Available               : ' + str(trin.is_available))
            if trin.is_available:
                log.info('    Enabled                 : ' + str(trin.enabled))
                log.info('    Kinds                   : ' + trigger_kind_str(trin.kinds))
                if trin.kinds != TKM_NONE:
                    log.info('    Kind                    : ' + trigger_kind_str(trin.kind))
            num += 1


def print_trigger_outputs_info(dev, log):
    if not isinstance(dev, Device):
        raise

    if len(dev.trigger_outputs) > 0:
        num = 0
        for trout in dev.trigger_outputs:
            log.info('  Trigger output {:d}:'.format(num))
            log.info('    Id                      : ' + str(trout.id))
            log.info('    Name                    : ' + str(trout.name))
            log.info('    Enabled                 : ' + str(trout.enabled))
            log.info('    Events                  : ' + trigger_output_event_str(trout.events))
            log.info('    Event                   : ' + trigger_output_event_str(trout.event))
            num += 1


def print_server_info(srv, log):
    if not isinstance(srv, Server):
        raise

    log.info('Server:')
    log.info('  URL                       : ' + srv.url)
    log.info('  Name                      : ' + srv.name)
    log.info('  Description               : ' + srv.description)
    log.info('  IPv4 address              : ' + ipv4_str(srv.ipv4_address))
    log.info('  IP port                   : ' + str(srv.ip_port))
    log.info('  Id                        : ' + srv.id)
    log.info('  Version                   : ' + str(srv.version) + srv.version_extra)
    log.info('  Status                    : ' + server_status_str(srv.status))
    if srv.last_error != SERVER_ERROR_NONE:
        log.info('  Last error                : ' + server_error_str(srv.last_error))
