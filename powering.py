#
# Wrapper around I2C interface to PiJuice UPS
#
# Documentation for I2C commands: https://github.com/PiSupply/PiJuice/blob/master/Software/README.md#i2c-command-api
from pijuice import PiJuice

class PiJuiceStatus:
    """ Class holding PiJuice status information """
    def __init__(self):
        """ initializes class and establishes communication with PiJuice via I2C interface """
        self.clear()

    def clear(self):
        """ sets all stored values to 'None' """
        self.charge = None
        self.temp = None
        self.vbat = None
        self.ibat = None
        self.vio = None
        self.iio = None
        self.isfault = None
        self.battery = None
        self.power_usb = None
        self.power_gpio = None


class PiJuiceWrapper:
    """ Convenience wrapper for I2C interface calls to PiJuice """
    def __init__(self, bus=1, address=0x14):
        """ initializes class and establishes communication with PiJuice via I2C interface """
        self.pijuice = PiJuice(bus, address)
        self.stat = PiJuiceStatus()
        self.update()


    def update(self):
        """updates status values from the PiJuice. Called once upon initialization.
        Raises ConnectionError in case of communication errors with the PiJuice module."""
        try:
            status = self.error_handler(self.pijuice.status.GetStatus())
            self.stat.isfault = status['isFault']
            self.stat.battery = status['battery']
            self.stat.power_usb = status['powerInput']
            self.stat.power_gpio = status['powerInput5vIo']
            self.stat.charge = self.error_handler(self.pijuice.status.GetChargeLevel())
            self.stat.temp = self.error_handler(self.pijuice.status.GetBatteryTemperature())
            self.stat.vbat = self.error_handler(self.pijuice.status.GetBatteryVoltage())
            self.stat.ibat = self.error_handler(self.pijuice.status.GetBatteryCurrent())
            self.stat.vio = self.error_handler(self.pijuice.status.GetIoVoltage())
            self.stat.iio = self.error_handler(self.pijuice.status.GetIoCurrent())
        except ConnectionError as e:
            # clear pot. stale values
            self.stat.clear()
            # pass exception on
            raise e

    def get_fault(self):
        return self.error_handler(self.pijuice.status.GetFaultStatus())

    def has_power(self):
        """ returns True if good power source is available on either GPIO pins or via USB, False otherwise. """
        if self.stat.power_gpio == 'PRESENT' or self.stat.power_usb == 'PRESENT':
            return True
        else:
            return False

    def error_handler(self, comstatus):
        """ unpacks communication status dictionaries returned by PiJuice and raises ConnectionError exception in case of errors """
        if comstatus['error'] == 'NO_ERROR':
            # no error: either return data or 'True'
            if 'data' in comstatus:
                return comstatus['data']
            else:
                return
        else:
            raise ConnectionError(f"PiJuice communication error: {comstatus['error']}")

    def reset_flags(self):
        """ resets fault status flags """
        self.pijuice.status.ResetFaultFlags(['powerOffFlag', 'sysPowerOffFlag'])

    def about_status(self):
        """ returns a summary on the general status (e.g. for logging) """
        return f"Battery: {self.stat.battery}, faults reported: {'None' if not self.stat.isfault else 'YES'}, {self.about_power()}"

    def about_power(self):
        """ returns a summary on the powering status (e.g. for logging) """
        return f"GPIO power state: {self.stat.power_gpio}, USB power state: {self.stat.power_usb}"

    def about_battery(self):
        """ returns a summary on battery status parameters (e.g. for logging) """
        return f"charged {self.stat.charge}%, T={self.stat.temp}° C, Vbat={self.stat.vbat} mV, Ibat={self.stat.ibat} mA, Vio={self.stat.vio} mV, Iio={self.stat.iio} mA"


def main():
    """ demonstration for using PiJuiceWrapper class """
    p = PiJuiceWrapper()
    if p.has_power():
        print('PiJuice is powered!')
    else:
        print('PiJuice is NOT powered!')
    print(p.about_status())
    print(p.about_battery())

if __name__ == '__main__':
    main()
