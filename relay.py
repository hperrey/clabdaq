#!/usr/bin/env python3
import RPi.GPIO as GPIO

class Relay:
    """ Class to control a 5V relay through a GPIO pin """
    def __init__(self, gpio):
        """ `gpio` gives the GPIO number of the pin the relay is connected to.  """
        # disable warnings about pin being in use
        # (could use GPIO.cleanup() in __del__ method,
        # but that results in a RuntimeError in GPIOBase on SIGINT/SIGTERM)
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers
        self.gpio = gpio
        GPIO.setup(self.gpio, GPIO.OUT) # GPIO Assign mode
        GPIO.output(self.gpio, GPIO.LOW)
        self.is_low = True # keeps state
   # this would set the used pins back to INPUT:
   # def __del__(self):
   #     # avoid warnings by setting OUTPUT pin to INPUT again
   #     GPIO.cleanup()
    def switch(self):
        """ Switches state of the relay. """
        if self.is_low:
            GPIO.output(self.gpio, GPIO.HIGH)
        else:
            GPIO.output(self.gpio, GPIO.LOW)
        # update state
        self.is_low = not self.is_low
    def setlow(self):
        """ Sets relay control pin to 'low' """
        GPIO.output(self.gpio, GPIO.LOW)
        self.is_low = True
    def sethigh(self):
        """ Sets relay control pin to 'high' """
        GPIO.output(self.gpio, GPIO.HIGH)
        self.is_low = False
    def is_low(self):
        return self.is_low


if __name__ == '__main__':
    import time
    print("Demo routine switching a relay on GPIO #27 from LOW to HIGH and back.")
    print("Starting in 10s...")
    time.sleep(10)
    r = Relay(27)
    print("Relay is initialized to low")
    time.sleep(10)
    print("Setting relay to HIGH")
    r.sethigh()
    time.sleep(10)
    print("Setting relay to LOW")
    r.setlow()
    print("done!")
