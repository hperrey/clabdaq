#!/usr/bin/env python3

from enum import Enum
import os.path
import datetime
from statistics import median

class SensorType(Enum):
    temperature = 1
    humidity = 2
    water = 3
    charge = 4
    voltage = 5
    current = 6
    diskspace = 7
    statusbit = 8

class Sensors:
    """ class to hold sensor data from various sources. Individual sensor information is stored in a dict. """

    def __init__(self):
        self.sensors = []
        self.mday_reset = datetime.datetime.now()
    def add(self, name, stype):
        """ adds a new sensor with given name and stype. The name should be unique to all sensors or a 'KeyError' exception will be raised. """
        try:
            # try to get entry with same name
            if self.get(name):
                raise KeyError(f"Entry with name '{name}' already exists")
        except StopIteration:
            # no entry found
            self.sensors.append({
                "name" : name,
                "stype" : stype,
                "data" : list(), # stores last X values (FIFO)
                "cur"  : None,   # current (i.e. last) value
                "median" : None, # median of 'data' list
                "maxday" : None, # max of median values (reset daily)
                "max"  : None    # max of median values (all time)
            })
    def get_all(self, stype):
        """ returns a list with all sensors of 'stype'"""
        return [s for s in self.sensors if s['stype'] == stype]
    def get(self, name):
        """ returns first sensor matching 'name'"""
        return next(s for s in self.sensors if s['name'] == name)
    def update(self, name, val):
        """updates recorded current value for sensor with 'name'. Will update median and recorded
        max values as well (based on median) if sufficient data has been collected."""
        try:
            entry = next(s for s in self.sensors if s['name'] == name)
        except StopIteration:
            raise StopIteration(f"Could not find entry '{name}' in sensors")
        entry['cur'] = val
        if not val is None and not isinstance(val, str):
            # data FIFO
            entry['data'].append(val)
            # remove entry if list gets too long
            if len(entry['data']) >= 10:
                entry['data'].pop(0) # remove first (=oldest) item
            # start calculating median and max values if at least 6 elements have been collected
            if len(entry['data']) >= 6:
                try:
                    m = median(entry['data'])
                except TypeError:
                    # something else.. something bad... has slipped through; just look away.
                    return
                entry['median'] = m
                if entry['maxday'] is None or entry['maxday'] < m:
                    entry['maxday'] = m
                if entry['max'] is None or entry['max'] < m:
                    entry['max'] = m
    def reset_maxday(self):
        """ resets the daily max values to the current median value. To be called at a fixed time during the day. """
        for s in self.sensors:
            s['maxday'] = s['median']
        self.mday_reset = datetime.datetime.now()
    def reset(self, name):
        """ resets the current values to None. Resets all entries where 'name' is found in sensor name. """
        for s in self.sensors:
            if name in s['name']:
                s['cur'] = None
                s['data'] = list()
                s['median'] = None
    def print(self, header = True):
        """ prints summary of information held """
        if header:
            print("   "+"\t".join([s['name'] for s in self.sensors]))
            print("   "+"\t".join(["cur/med/max/maxday" for s in self.sensors]))
        print("   ", end = '')
        for s in self.sensors:
            try:
                print(f"{s['cur']:.01f}/{s['median']:.01f}/{s['max']:.01f}/{s['maxday']:.01f} \t", end='')
            except TypeError:
                print(f"None\t\t\t", end='')
        print(" ")
    def write(self, fname):
        """ writes current values to csv file specified by 'filename'"""
        # check whether file exists and write header if not
        if not os.path.isfile(fname):
            with open(fname, 'w') as f:
                # should write header first
                f.write("\"time\",")
                f.write(",".join([f"\"{s['name']}\"" for s in self.sensors])+'\n')
        # now append sensor data
        with open(fname, 'a') as f:
            f.write(str(datetime.datetime.now())+",")
            f.write(",".join(map(str,[s['cur'] for s in self.sensors]))+'\n')


def main(counts = 30):
    # demo
    sensors = Sensors()
    sensors.add("daqbox_temp", SensorType.temperature)
    sensors.add("linkbox_temp", SensorType.temperature)
    sensors.add("pijuice", SensorType.temperature)
    sensors.add("daqbox_hum", SensorType.humidity)
    sensors.add("linkbox_hum", SensorType.humidity)
    sensors.add("det. box 1", SensorType.water)
    sensors.add("det. box 2", SensorType.water)
    sensors.add("charge", SensorType.charge)
    sensors.add("sd", SensorType.diskspace)
    sensors.add("hdd", SensorType.diskspace)
    sensors.add("key", SensorType.diskspace)
    # add HV sensors
    for ch in range(4):
        sensors.add(f"imon_ch{ch}", SensorType.current)
        sensors.add(f"vmon_ch{ch}", SensorType.voltage)
    # loop and fill with random data
    import random
    count = 0
    while True:
        for s in sensors.get_all(SensorType.voltage):
            r = random.uniform(1, 2000)
            sensors.update(s['name'], r)
        for s in sensors.get_all(SensorType.current):
            r = random.uniform(1, 500)
            sensors.update(s['name'], r)
        r = random.uniform(1, 100)
        sensors.update('daqbox_temp', r)
        r = random.uniform(1, 100)
        sensors.update('daqbox_hum', r)
        r = random.uniform(1, 100)
        sensors.update('linkbox_hum', r)
        r = random.uniform(1, 100)
        sensors.update('linkbox_temp', r)
        r = random.uniform(1, 100)
        sensors.update('pijuice', r)
        r = random.uniform(1, 100)
        sensors.update('charge', r)
        r = random.uniform(1, 100)
        sensors.update('sd', r)
        r = random.uniform(1, 100)
        sensors.update('hdd', r)
        r = random.uniform(1, 100)
        sensors.update('key', r)

        r = random.uniform(1, 100)
        if r < 50:
            sensors.update('det. box 1', False)
        else:
            sensors.update('det. box 1', True)
        r = random.uniform(1, 100)
        if r < 50:
            sensors.update('det. box 2', False)
        else:
            sensors.update('det. box 2', True)
        if count == 15:
            print("resetting maxday!")
            sensors.reset_maxday()
        if count == 15:
            print("resetting linkbox_temp!")
            sensors.reset('linkbox_temp')
        sensors.print(header = True if count == 0 else False)
        sensors.write("sensors_test.csv")
        if count == counts:
            break
        count += 1
    print("done with sensor tests!")
    return sensors
    
if __name__ == '__main__':
    main()
