#!/bin/bash
# This script is used by the pijuice daemon to signal
# our DAQ when power from mains is lost.
kill -s SIGUSR1 `cat /home/pi/clabdaq/run_clabdaq.pid`
touch /home/pi/clabdaq/pwrloss
