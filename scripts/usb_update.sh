#!/usr/bin/env bash
set -euo pipefail

# set remote and branch name
REMOTE="clabdaq_usb"
BRANCH="master"

# enter directory where clabdaq is installed
cd /home/pi/clabdaq

# change the lamp state to indicate USB connection
# color: white
python3 set_led.py 1.0 1.0 1.0

# update our repository from the remote
if ! git fetch --quiet $REMOTE; then
    echo "ERROR $? when fetching from remote repository $REMOTE!"
    # change the lamp state to indicate error
    # color: purple
    python3 set_led.py 0.7 0 0.99
    sleep 3
else
    # is there anything to merge?
    # count changes that the remote repository is AHEAD of us
    if [ $(git rev-list HEAD.."$REMOTE/$BRANCH" --count) -gt 0 ]; then
        # yes there is
        if ! git merge --quiet "$REMOTE/$BRANCH"; then
            echo "ERROR $? when merging changes from branch $BRANCH on $REMOTE!"
        else
            echo "Updated repository! :)"
            sudo reboot
        fi
    else
        # no there is nothing to merge
        echo "No update available"
    fi
fi

# wait
sleep 2

# change the lamp state to indicate success
# color: green
python3 set_led.py 0 1.0 0

# now unmount the device again
sudo umount /media/usb_auto
