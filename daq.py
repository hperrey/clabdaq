#!/usr/bin/env python3
#
# Data Acquisition routines
#
# Take data of an TiePie Handyscope HS6 in streaming mode
#

import time
import libtiepie
import printinfo
import logging
import argparse
import configparser

class Errors(Exception):
    """Base class for exceptions in this module."""
    pass

class CommError(Errors):
    """Exception raised when communication with device returns with error msg or fails.

    Attributes:
        message -- explanation of the alert
            par -- the parameter sent to the device that caused the error
    """

    def __init__(self, msg, par):
        self.par = par
        self.msg = msg
        # Call the base class constructor with the parameters it needs
        super().__init__(msg)


class LogFilter(object):
    """ Class to filter log messages for a specified, exclusive log level """
    def __init__(self, level):
        self.__level = level

    def filter(self, logRecord):
        return logRecord.levelno <= self.__level

class TriggerConf:
    """ class to channel-by-channel hold trigger configuration parameters """
    def __init__(self, enabled = True, level = 0.5, kind = libtiepie.TK_RISINGEDGE, hystereses = 0.05):
        self.enabled = enabled
        self.level = level # in %; can be a list as some modes support several levels
        self.kind = kind
        self.hystereses = hystereses # can be a list as some modes support several levels

class ChannelConf:
    """ class to hold by-channel configuration parameters for Oscilloscope """
    def __init__(self, enabled = True, vrange = 8, bandwidth = 250000000, coupling = libtiepie.CK_DCV, safeground = True):
        # Enable channel to measure it:
        self.enabled = enabled
        self.vrange = vrange  # in V
        self.coupling = coupling  # DC Volt
        self.bandwidth = bandwidth # in Hz
        self.safeground = safeground # bool; enables Safe Ground (single-ended connection)
        self.probe_offset = 0 # in Volt, Ampere or Ohm, depending on the input coupling; does not affect oscilloscope operation nor raw mode!
        self.trigger = TriggerConf()

class Configuration:
    """ class to hold configuration parameters for Oscilloscope """
    def __init__(self, nch = 4):
        self.measure_mode = libtiepie.MM_BLOCK # libtiepie.MM_STREAM or libtiepie.MM_BLOCK
        self.sample_frequency = 1e6  # in Hz
        self.record_length = 1000
        self.resolution = 8 # in bits
        self.trigger_timeout = libtiepie.TO_INFINITY # in s
        self.raw = True # whether or not to read data in raw mode (uint) instead for voltages (floats)
        self.ch = []
        for c in range(nch):
            self.ch.append(ChannelConf())
    def getch(self, chno):
        """ returns given channel's config """
        return self.ch[chno]
    def enable_channel(self, chno):
        """ enables a channel """
        self.ch[chno].enabled = True
    def enable_trigger(self, chno):
        """ enables a channel's trigger """
        self.ch[chno].trigger.enabled = True
    def disable_channel(self, chno):
        """ disables a channel and it's trigger """
        self.ch[chno].enabled = False
        self.ch[chno].trigger.enabled = False
    def disable_channels(self):
        """ disables all channels and their trigger """
        for chno in range(len(self.ch)):
            self.disable_channel(chno)

def readConfig(filename):
    """ reads a configuration from filename and sets up a Configuration object """
    defaults = {
        'max_wait_data' : 60,
        'measure_mode' : "block",
        'sample_frequency' : int(1e6),
        'record_length' : 1000,
        'resolution' : 8,
        'raw' :  True,
        'enabled' :  True,
        'coupling' : 'DCV',
        'vrange' :  8,
        'safeground' :  True,
        'probe_offset' :  0,
        'bandwidth' : 250000000,
        'trigger_enabled' :  True,
        'trigger_type' :  'falling edge',
        'trigger_level' :  50,
        'trigger_hystereses' :  0.05
    }
    cfgfil = configparser.ConfigParser(defaults=defaults, default_section='channel defaults')
    cfgfil.read(filename)
    device = cfgfil['device']
    # what is the max. channel number that has been configured?
    if 'nchannels' in device:
        nch = device.getint('nchannels')
    else:
        # guess from the max. number of ch section instead
        try:
            maxch = max([int(s) for sec in cfgfil.sections() if sec.startswith('channel') for s in sec.split() if s.isdigit()])
            nch = maxch +1
        except ValueError:
            # none given in config and no ch sections, use default
            nch = 4
    # use this number+1 as nch
    # if this assumption is wrong, then we leave higher ch unconfigured
    cfg = Configuration(nch=nch)
    # set devices values
    cfg.max_wait_data = device.getint('max_wait_data')  # in s
    cfg.measure_mode = libtiepie.MM_STREAM if "stream" in device.get('measure_mode') else libtiepie.MM_BLOCK
    cfg.sample_frequency = device.getint('sample_frequency')  # in Hz
    cfg.record_length = device.getint('record_length')
    cfg.resolution = device.getint('resolution') # in bits
    try:
        cfg.trigger_timeout = cfgfil.getint('device', 'trigger_timeout') # in s
    except (configparser.NoOptionError, ValueError):
        cfg.trigger_timeout = libtiepie.TO_INFINITY # disable timeout instead
    cfg.raw = device.getboolean('raw')
    def liblookup(what, cfg, libdict):
        """searches for 'what' defined in 'cfg' for matches in library dictionary
        'libdict' mapping int to strings. Allows 'fallback' if no entry is found in
        config. Raises ValueError if more than one match found. """
        search = cfg.get(what)
        matches = [bit for bit, desc in libdict.items() if search.lower() in desc.lower()]
        if len(matches) != 1:
            candidates = [desc for bit, desc in libdict.items() if search.lower() in desc.lower()]
            raise ValueError(f"No or too many matches for '{search}' as {what} (candidates: "+', '.join(map(str, candidates))+')')
        return matches[0]
    # per channel config:
    for i, c in enumerate(cfg.ch):
        try:
            sec = cfgfil[f'channel {i}']
        except KeyError:
            # hop overy this entry
            continue
        c.enabled = sec.getboolean('enabled')
        c.vrange = sec.getfloat('vrange')  # in V
        c.coupling = liblookup('coupling', sec, libtiepie.COUPLINGS)  # DC Volt
        c.safeground = sec.getboolean('safeground') # bool; enables Safe Ground (single-ended connection)
        c.probe_offset = sec.getint('probe_offset') # in Volt, Ampere or Ohm, depending on the input coupling; does not affect oscilloscope operation nor raw mode!
        c.bandwidth = sec.getint('bandwidth')
        c.trigger.enabled = sec.getboolean('trigger_enabled')
        c.trigger.level = sec.getint('trigger_level') # in %; TODO support that it can be a list as some modes support several levels
        c.trigger.kind = liblookup('trigger_kind', sec, libtiepie.TRIGGER_KINDS)
        c.trigger.hystereses = sec.getfloat('trigger_hystereses') # TODO support that this can be a list as some modes support several levels
    return cfg

class Oscilloscope:
    """ class to interact with TiePie Handyscope HS6 DIFF """
    def __init__(self, config = Configuration()):
        self.scp = None
        self.log = logging.getLogger('root')
        self.raw = True
        self.max_wait_dev = 5 # max time to wait for device to be ready (in s)
        self.max_wait_data = 60 # max time to wait for data to be ready (in s)
        self.config = config # NOTE number of channels might not be correct in config (not read from device)
        # TODO connect to device and retrieve (default) config from there first
    def connect(self):
        """Try to open oscilloscope with stream measurement support."""
        # Search for devices:
        libtiepie.device_list.update()

        for item in libtiepie.device_list:
            if item.can_open(libtiepie.DEVICETYPE_OSCILLOSCOPE):
                self.log.info('  Found device: ' + item.name)
                self.log.info('  Serial number  : ' + str(item.serial_number))
                self.log.info('  Available types: ' + libtiepie.device_type_str(item.types))

                self.scp = item.open_oscilloscope()
                # check for stream support
                if self.scp.measure_modes & self.config.measure_mode:
                    break
                else:
                    self.log.error("Device does not support selected measurement mode!")
                    self.scp = None
    def configure(self):
        """ Configure device """
        # TODO add parameter with config object to get the settings from
        if not self.scp:
            self.log.error("Cannot configure oscilloscope: No device connected")
            return
        # Set timeout for data to be ready on device
        self.max_wait_data = self.config.max_wait_data
        # Set whether or not to use raw mode (library setting)
        self.raw = self.config.raw
        # Set measure mode:
        self.scp.measure_mode = self.config.measure_mode
        # Set resolution:
        self.scp.resolution = self.config.resolution
        # For all channels:
        for idx, ch in enumerate(self.config.ch):
            self.log.debug(f"Configuring channel {idx}")
            # Enable channel to measure it:
            self.scp.channels[idx].enabled = ch.enabled
            # Set range:
            self.scp.channels[idx].range = ch.vrange
            # Set bandwidth:
            self.scp.channels[idx].bandwidth = ch.bandwidth
            # Set coupling:
            self.scp.channels[idx].coupling = ch.coupling
            # set offset
            self.scp.channels[idx].probe_offset = ch.probe_offset
            # Set trigger options
            self.scp.channels[idx].trigger.enabled = ch.trigger.enabled
            self.scp.channels[idx].trigger.kind = ch.trigger.kind
            # Some trigger modes can support several levels in their configuration
            if isinstance(ch.trigger.level, list):
                for l in range(len(ch.trigger.level)):
                    self.scp.channels[idx].trigger.levels[l] = ch.trigger.level[l]
                else:
                    self.scp.channels[idx].trigger.levels[0] = ch.trigger.level
            # same for hystereses:
            if isinstance(ch.trigger.hystereses, list):
                for l in range(len(ch.trigger.hystereses)):
                    self.scp.channels[idx].trigger.hystereses[l] = ch.trigger.hystereses[l]
                else:
                    self.scp.channels[idx].trigger.hystereses[0] = ch.trigger.hystereses
            # Set Safe Ground:
            if self.scp.channels[idx].has_safe_ground:
                self.scp.channels[idx].safe_ground_enabled = ch.safeground
        # Set sample frequency:
        # NOTE need to do mode, resolution, channel enable config before setting frequency
        self.scp.sample_frequency = self.config.sample_frequency
        # Set record length:
        self.scp.record_length = self.config.record_length
        # Set trigger timeout
        self.scp.trigger_time_out = self.config.trigger_timeout
        # Check that we have a config for each channel:
        if len(self.scp.channels) != len(self.config.ch):
            self.log.warning(f"Number of configured channels ({len(self.config.ch)}) does not match number of channels present in device ({len(self.scp.channels)})!")

    def printinfo(self):
        printinfo.print_device_info(self.scp, self.log)

    def acquire(self):
        """ acquires a sample and returns it as list of lists (one list per channel) """
        if not self.scp:
            self.log.error("Cannot acquire data from oscilloscope: No device connected")
            return
        # TODO check that device is configured (or redo configuration)
        # TODO check that the device is (still) connected
        self.log.info("Starting Acquisition")
        data = None
        try:
            # Start measurement:
            self.scp.start()

            # Wait for measurement to start:
            timeout = 0
            delay = 0.01
            while not self.scp.is_running:
                time.sleep(delay)  # delay (in s), to save CPU time
                timeout = timeout + delay
                if timeout > self.max_wait_dev:
                    self.log.error("Timeout reached (max_wait_dev) while waiting for DEVICE to be ready.")
                    break # TODO add exception here

            if self.scp.is_running:
                self.log.info("Acquisition is running")

            # Force trigger if in BLOCK mode
            if self.config.measure_mode == libtiepie.MM_BLOCK:
                self.log.info("Sending software trigger")
                self.scp.force_trigger()

            self.log.info("Waiting for data")

            # Wait for measurement to complete:
            timeout = 0
            overflow_seen = False
            delay = 0.0001
            while not self.scp.is_data_ready:
                # NOTE too long delay can lead to "overflow" condition
                time.sleep(delay)  # delay (in s), to save CPU time
                if self.scp.is_data_overflow and not overflow_seen:
                    # TODO investigate how to handle this properly
                    self.log.error('Data overflow registered!')
                    overflow_seen = True
                timeout = timeout + delay
                if timeout > self.max_wait_data:
                    self.log.error("Timeout reached (max_wait_data) while waiting for data to be ready.")
                    break

            # identify trigger if running in block mode
            if self.config.measure_mode == libtiepie.MM_BLOCK and self.scp.is_triggered:
                whytrig = 'Forced' if self.scp.is_force_triggered else 'timeout' if self.scp.is_time_out_triggered else 'other'
                self.log.info(f"Trigger seen. Type: {whytrig}")

            # Get data as numpy array:
            try:
                data = self.scp.get_data(raw=self.raw)
            except Exception as e:
                self.log.error(f"{type(e).__name__}: {e}")
                import traceback
                self.log.info(traceback.format_exc())


            # Stop stream:
            if self.config.measure_mode == libtiepie.MM_STREAM:
                self.scp.stop()

        except Exception as e:
            self.log.error(f"{type(e).__name__}: {e}")
            import traceback
            self.log.info(traceback.format_exc())
            # TODO add actual handling of the exception; stop acquisition?

        return data

    def close(self):
        """ closes the connection to the oscilloscope """
        if self.scp:
            del self.scp


def singleChRead(cfgfn, ch):
    """Records data from the oscilloscope for a single active channel 'ch' and
    returns list with data. Takes config filename to load settings from as
    argument

    """
    # TODO register (and unregister) log handler to write log info for just this run in a separate file
    # read settings from file:
    conf = readConfig(cfgfn)
    # disable all channels:
    conf.disable_channels()
    # enable a single channel:
    conf.enable_channel(ch)
    # set up oscilloscope:
    d = Oscilloscope(config = conf)
    d.connect()
    if d.scp is None:
        # no connection established
        raise CommError(msg="Could not connect to oscilloscope",par="connect")
    d.configure()
    d.printinfo()
    data = d.acquire()[ch] # reduce to list of active channel
    d.close()
    return data


def run(ch=0):
    """ demo/test of module """
    log = logging.getLogger('root')
    handler_stream = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(module)s(%(levelname)s): %(message)s',"%d.%m. %H:%M:%S")
    timestr = time.strftime("%Y%m%d-%H%M%S")
    handler_stream.setFormatter(formatter)
    log.addHandler(handler_stream)
    numeric_level = getattr(logging, "INFO",
                            None)  # default: INFO messages and above
    log.setLevel(numeric_level)
    log.info("now running")
    # read data from a single channel
    data = None
    # TODO identify and catch specific exceptions!
    try:
        data = singleChRead("./config/oscilloscope.ini", ch)
    except Exception as e:
        log.exception('Caught exception during file writing: ' + str(e))
    # save data to file
    fn = f"run_ch{ch}_{timestr}"
    # open binary file for writing and store acquired data there:
    # TODO identify and catch specific exceptions!
    try:
        if not data is None:
            with open(fn, 'wb') as f:
                data.tofile(f)
                log.info(f"Wrote data to {fn}")
        else:
            log.error("No data read out to write!")
    except Exception as e:
        log.exception('Caught exception during file writing: ' + str(e))
    log.info("Done!")

def parse_arguments():
    parser = argparse.ArgumentParser(description='TiePie Handyscope HD0 based DAQ application.')
    parser.add_argument('-ch', '--channel', help="channel number to acquire data from",
                        type=int, default=0
    )

    return parser.parse_args()

if __name__ == '__main__':
    args = parse_arguments()
    run(ch=args.channel)
