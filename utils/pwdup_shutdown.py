#!/usr/bin/env python3
#
# Enable wake up on power re-connect: script shuts down
# Raspberry Pi but enables wake-up-on-charge of the pijuice beforehand.
# See https://github.com/PiSupply/PiJuice/issues/176

import pijuice
import os
pj=pijuice.PiJuice(1, 0x14)
pj.power.SetWakeUpOnCharge(0)
os.system("sudo halt")
