#!/usr/bin/env python3
#
# Monitor pijuice status by reading out relevant parameters and writing to screen (and file)
#

from pijuice import PiJuice
import time

pijuice = PiJuice(1,0x14)

print(f"date, temp, vbat, ibat, vio, iio, charge")
while True:
    temp =  pijuice.status.GetBatteryTemperature()
    temp = temp['data'] if temp['error'] == 'NO_ERROR' else temp['error']
    vbat = pijuice.status.GetBatteryVoltage()
    vbat = vbat['data'] if vbat['error'] == 'NO_ERROR' else vbat['error']
    ibat = pijuice.status.GetBatteryCurrent()
    ibat = ibat['data'] if ibat['error'] == 'NO_ERROR' else ibat['error']
    vio =  pijuice.status.GetIoVoltage()
    vio = vio['data'] if vio['error'] == 'NO_ERROR' else vio['error']
    iio = pijuice.status.GetIoCurrent()
    iio = iio['data'] if iio['error'] == 'NO_ERROR' else iio['error']
    charge = pijuice.status.GetChargeLevel()
    charge = charge['data'] if charge['error'] == 'NO_ERROR' else charge['error']

    timestr = time.strftime("%Y%m%d-%H%M%S")
    print(f"{timestr}, {temp}, {vbat}, {ibat}, {vio}, {iio}, {charge}")
    with open('pwdcons.csv', 'a') as f:
        f.write(f"{timestr}, {temp}, {vbat}, {ibat}, {vio}, {iio}, {charge}\n")
    time.sleep(1)
