#!/usr/bin/env python3
#
# Provides classes to handle alerts coming from external signals or interrupts.
#

import datetime
import signal


class Alerts(Exception):
    """Base class for exceptions in this module."""
    pass

class BatteryAlert(Alerts):
    """Exception raised when battery operation has been signaled.

    Attributes:
        frameinfo -- details on the stack frame in which the alert occurred
        message -- explanation of the alert
    """
    def __init__(self, frameinfo, message):
        self.frameinfo = frameinfo
        self.message = message
        # Call the base class constructor with the parameters it needs
        super().__init__(message)

class WaterAlert(Alerts):
    """Exception raised when water has been detected by one of the sensors.

    Attributes:
        where -- details on the location of the sensor in which the alert occurred
        message -- explanation of the alert
    """
    def __init__(self, where, message):
        self.where = where
        self.message = message
        # Call the base class constructor with the parameters it needs
        super().__init__(message)

class AlertHandler:
    """ Class to handle alerts triggered by external signals or interrupts.
    """

    def __init__(self):
        self.raises = False # wether or not to raise exceptions on alerts
        self.reset()
        self.last_alert = None # persistent value only updated when alarm is raised (but not affected by reset)
    def register_alert(self, where, exception):
        self.alert = True
        self.where = where
        self.when = datetime.datetime.now()
        self.last_alert = self.when
        if self.raises:
            raise exception
    def seen_alert(self):
        return self.alert
    def reset(self):
        self.alert = False
        self.when = None
        self.where = None


class WaterAlertHandler(AlertHandler):
    """ Class to handle alerts triggered by water sensors.
    """
    def register_alert(self, where):
        super().register_alert(where, WaterAlert(self.where, f"Water sensor in {self.where} triggered"))


class BatteryAlertHandler(AlertHandler):
    """ Class to handle signals indicating when battery operation has been entered.
    """

    def __init__(self):
        # Set the signal handler for SIGUSR1 which we will use to indicate the pijuice going to battery mode
        self.original_handler = signal.signal(signal.SIGUSR1, self.register_alert)
        super().__init__()
    def register_alert(self, signum, frame):
        where = "Frame %s in %s at line %s" % (frame.f_code.co_name,
                                                    frame.f_code.co_filename,
                                                    frame.f_lineno)
        super().register_alert(where, BatteryAlert(self.where, f"Entered battery operation"))
