#!/usr/bin/env python3
#
# main routine to run the different compoents of the piDAQ together
#

import os
import time
import sys
import argparse
import configparser
import datetime
import logging
import shutil
import random
import subprocess
import traceback
import signal

from gpiozero import CPUTemperature
import board
import adafruit_dht
import socket

import serial.serialutil

import sdnotify

import logsetup
import daq
import hv
import sensor
import watersensor
import powering
import alerts
import state
import display
import relay

def parse_arguments():
    parser = argparse.ArgumentParser(description='main routine to run the different compoents of the piDAQ together')
    parser.add_argument('-c', '--config', help="Which configuration file to use",
                        default="./config/main.ini", metavar="FILE"
                        )

    return parser.parse_args()

class SignalHandler:
    halt_now = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)
    def exit_gracefully(self, signum, frame):
        print('CLABDAQ received signal to terminate')
        self.halt_now = True
        # Raises SystemExit(0):
        sys.exit(0)

def feed_watchdog():
    sdn = sdnotify.SystemdNotifier()
    sdn.notify("WATCHDOG=1")

def nextRun(thisRun, startTime, stopTime):
    """determine start of next daq run from prev. run's timestamp `thisRun`
    (datetime object). `startTime` and `stopTime` are integers
    indicating the hours of the time window in which the DAQ is
    active. The scheduled run will be randomly placed in the
    next open window.

    """
    # begin of active time window today
    b = thisRun.replace(hour=startTime, minute=0, second=0)
    if b < thisRun:
        # need to shift by a day
        b = b + datetime.timedelta(hours=24)
    # determine length of active daq window
    if startTime < stopTime:
        l = stopTime-startTime
    else:
        # time window crosses midnight:
        l = 24-startTime + stopTime
    # now find a random hour after the start of the daq period and before the end:
    min = random.randint(0, l*60)
    res = (b +
           datetime.timedelta(
               minutes=min,
               seconds=random.randint(0, 60)))
    return res

def setLedColor(r, g, b):
    """connects to the locally-running pilamp server and requests a LED color
    change to the r, g, b (fractional) values specified as parameters."""
    log = logging.getLogger('root')
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', 12345) # default port for pilamp server
    # set timeout
    sock.settimeout(1) # tcp connection will take time
    # establish connection
    try:
        sock.connect(server_address)
        time.sleep(0.1)
    except Exception as e:
        log.error('Error communicating with pilamp server: %r' %e)
        return
    try:
        buffer = sock.recv(1024).decode()
        log.info(f'Setting LED color to {r} {g} {b}')
        msg = f'set_rgb {r} {g} {b}'
        sock.sendall(msg.encode())
        time.sleep(0.1)
        buffer = sock.recv(1024).decode()
    except socket.error as e:
        log.error('Error communicating with pilamp server: %r' % e)
        return
    if buffer.startswith('OK'):
        log.info(f"Got response from pilamp server: 'OK'")
    # close connection
    sock.close()

def setupSensors(dht11):
    """Sets up and returns `sensor` object to hold our sensor data in. `dht11` is a
    dictionary of 'name:DHT11 sensor'self."""
    # instantiate class to hold our sensor data
    sens = sensor.Sensors()
    sens.add("pijuice", sensor.SensorType.temperature) # pijuice temperature sensor
    sens.add("battery", sensor.SensorType.charge) # raspberry pi cpu temperature
    sens.add("cpu", sensor.SensorType.temperature) # raspberry pi cpu temperature
    # initiate a dictionary with descriptions and the dht11 interfaces, with data pin connected to:
    for name in dht11:
        sens.add(name+'_temp', sensor.SensorType.temperature)
        sens.add(name+'_hum', sensor.SensorType.humidity)
    # add water sensors
    sens.add(f"det. box 1", sensor.SensorType.water)
    sens.add(f"det. box 2", sensor.SensorType.water)
    # add HV sensors
    for ch in range(4):
        sens.add(f"imon_ch{ch}", sensor.SensorType.current)
        sens.add(f"vmon_ch{ch}", sensor.SensorType.voltage)
        sens.add(f"stat_ch{ch}", sensor.SensorType.statusbit)
    sens.add(f"bdalarm", sensor.SensorType.statusbit)
    # add 'sensor' for tracking disk usage statistics
    sens.add("sd", sensor.SensorType.diskspace)
    sens.add("hdd", sensor.SensorType.diskspace)
    sens.add("key", sensor.SensorType.diskspace)
    return sens

class Application:
    """ CLABDAQ application class. Ties components together. """
    def __init__(self, config, log):
        # set up logging
        self.log = log
        # keep config around
        self.config = config
        # start the state machine
        # defaults to STATE: UNINITIALIZED
        self.statem = state.AutoDaqMachine()
        # write process' pid to file (to find target for sending signals)
        self.log.info(f"Process' PID is: {os.getpid()}")
        with open(self.config.get('general', 'pid_file'), 'w') as f:
            f.write(str(os.getpid()))
        hvcfg_fn = os.path.join(self.config.get('general', 'config_path'),
                                self.config.get('hv', 'config_file'))
        self.hvcfg = hv.loadConfig(hvcfg_fn)
        # set up interface to pijuice
        self.pijuice = powering.PiJuiceWrapper()
        # set up handler for receiving alerts issued by the pijuice system daemon
        self.bat = alerts.BatteryAlertHandler()
        # set up water sensors
        self.wap = watersensor.WaterAlarmPanel()
        self.wap.addSensor("det. box 1", 22)
        self.wap.addSensor("det. box 2", 23)
        # set up an alarm handler for the water sensors
        self.wat = alerts.WaterAlertHandler() # handling water sensor trigger
        nowat = alerts.WaterAlertHandler() # handling 'no more water' events
        self.wap.whenOpened(self.wat.register_alert) # alert if water detected
        self.wap.whenClosed(nowat.register_alert) # alert if no water detected anymore
        self.wap.softTrigger() # react to the current state
        # DHT11 temperature/humidity sensors
        self.dht11 = {
            "daqbox" : adafruit_dht.DHT11(board.D24),
            "linkbox" : adafruit_dht.DHT11(board.D25),
        }
        # prepare object holding sensor information:
        self.sens = setupSensors(self.dht11)
        # set up relay control
        self.psu_relay = relay.Relay(27) # GPIO #27
        # schedule the next data acquisition run:
        self.scheduled_daq = (datetime.datetime.now() +
                              datetime.timedelta(
                                  minutes=self.config.getint('hv', 'warmup_duration')))
        # time window and delay/wait time to use when scheduling next run:
        if self.config.getboolean('general', 'continuous_mode'):
            # run continuously, acquiring as much data as possible with minimal delay
            self.daq_active = { "continuous" : True }
        else:
            # normal operation: active during a time window, acquire once a day
            self.daq_active = {
                "start" :  self.config.getint('general', 'daq_active_start'),
                "stop" :  self.config.getint('general', 'daq_active_stop'),
                "continuous" : False }
        # record last time we ran (init here only)
        self.last_daq = datetime.datetime(day=5, month=11, year=1955, hour=6, minute=38, second=0, microsecond=0)
        # load run number from file
        self.runno = 0
        self.runnofn = self.config.get('general', 'runno_file')
        try:
            with open(self.runnofn, 'r') as f:
                self.runno = int(f.read())
        except FileNotFoundError:
            self.log.warning(f"No previously stored run number found in {self.runnofn}")
        except ValueError:
            self.log.error(f"Run number stored in {self.runnofn} is misformated!")
        self.log.info(f"Next run number is: {self.runno}")
        # set up display
        self.dis = display.Display(
            # file names used to log faults (warnings+errors) and alarms (critical)
            faultfn = os.path.join(self.config.get('general', 'log_path'), "faults.log"),
            alarmfn = os.path.join(self.config.get('general', 'log_path'), "alarms.log")
        )
        self.dis.clear()
        # change state to DETECTORSCOLD as long as the user hasn't enabled
        # 'passive_mode' in the config where we leave HV off and do not take data:
        if self.config.getboolean('general', 'passive_mode'):
            self.statem.passiveModeEnabled()
        else:
            self.statem.configured()

        # TODO DELME this is a temporary piece of code
        # remove old data to stop the HDD from filling up
        try:
            import glob
            # mount the hdd
            if not os.path.ismount("/media/hdd"):
                subprocess.check_call(["mount", "/media/hdd"])
            old_files = [glob.glob(f"/media/hdd/run_{i:06d}*") for i in range(691, 733)]
            # old_files is a list of lists, flatten it:
            # (we also want to check that the file exists with isfile)
            del_files = [f for flist in old_files for f in flist if os.path.isfile(f)]
            if del_files:
                for f in del_files:
                    self.log.info(f"Removing old file {f}")
                    os.remove(f)
            else:
                self.log.info("No old data to remove")
            # umount drive:
            subprocess.check_call(["umount", "/media/hdd"])
        except:
            self.log.warning("Exception occured trying to remove old data")
        # TODO DELME end temporary code segment

        self.log.info("System configured")
        setLedColor(0, 1, 0) # green
        # Inform systemd that we've finished our startup sequence...
        sdn = sdnotify.SystemdNotifier()
        sdn.notify("READY=1")

    def grace_sleep(self, grace_period):
        # skip this step should the display not be active:
        # (pausing w/o user feedback seems a bad idea)
        if self.config.getboolean('general', 'disable_display'):
            return
        # wait for a grace period if configured
        if grace_period > 0:
            self.log.info(f'Entering grace period of {grace_period} minutes')
            # helper function to keep the user entertained while waiting :)
            def splashscreen(m):
                self.dis.clear()
                self.dis.message(f'System configured.', pos=[15, 40], size=35)
                self.dis.message(f'DAQ will start in {m} minutes.', pos=[15, 55], size=35)
                self.dis.draw()
            # break up the grace period in single minutes so that we can feed the
            # watchdog regularly
            slept = 0
            while slept<grace_period:
                splashscreen(grace_period-slept)
                # feed watchdog
                feed_watchdog()
                time.sleep(60)
                slept += 1

    def update_sensors(self):
        """ updates data from all sensors and writes info to file. """
        # update pijuice status
        try:
            self.pijuice.update()
        except ConnectionError as e:
            self.log.error(f"{type(e).__name__}: {e}")
            self.log.info(traceback.format_exc())

        # update the recorded values:
        # refreshes max. values too
        cpu = CPUTemperature()
        self.sens.update('cpu', cpu.temperature)
        self.sens.update('pijuice', self.pijuice.stat.temp)
        self.sens.update('battery', self.pijuice.stat.charge)

        # temp/humidity
        for name, s in self.dht11.items():
            # some readings give checksum errors,
            # therefore, we want to give it a few tries before giving up
            attempts = 0
            value_read = False
            while not value_read and attempts < 5:
                try:
                    self.sens.update(name+'_temp', s.temperature)
                    self.sens.update(name+'_hum', s.humidity)
                    value_read = True
                except RuntimeError as e:
                    self.log.info(f"Could not read DHT11 sensor(s) '{name}': {e}")
                    attempts += 1
                    time.sleep(0.1)
                    continue
            if not value_read:
                self.log.error(f'Could not read DHT11 sensor "{name}" after {attempts} attempts.')
                self.sens.update(name+'_temp', None)
                self.sens.update(name+'_hum', None)

        # update water sensors
        for s in self.wap.sensors:
            self.sens.update(s.name, self.wap.isSensorOpen(s)) # True if water detected

        # update HV readings
        hvval = dict()
        if self.pijuice.has_power():
            try:
                hvval = hv.getValues(self.hvcfg)
            except hv.CmdError as e:
                self.log.error(f"{type(e).__name__}: {e}")
                self.log.info(traceback.format_exc())
            except serial.serialutil.SerialException as e:
                self.log.error(f"{type(e).__name__}: {e}")
                self.log.info(traceback.format_exc())
        # now update values and use None as fallback
        self.sens.update(f"bdalarm", hvval.get('BDALARM', None))
        for ch in range(4):
            chval = hvval.get(f'channel {ch}', dict())
            self.sens.update(f"imon_ch{ch}", chval.get('IMON', None))
            self.sens.update(f"vmon_ch{ch}", chval.get('VMON', None))
            self.sens.update(f"stat_ch{ch}", chval.get('STAT', None))


        # update disk space values for mounted directories
        total, used, free = shutil.disk_usage('/')
        self.sens.update('sd', 100*free/total)
        # write sensor data to file
        # -------------------------
        self.sens.write(self.config.get('general', 'sensor_stats_file'))

    def update_state(self):
        """ checks whether state change requirements are met and updates state machine. """
        # -> batteryPwd if we have received a signal from the pijuice daemon
        if self.bat.seen_alert() and not self.statem.is_batteryPwd:
            self.log.critical(f"Power loss signaled {self.bat.when}. Previous state: {self.statem.current_state.name} ({self.statem.get_strduration()})")
            self.statem.pwrLoss()

        # -> going back to power?
        if self.statem.is_batteryPwd and self.pijuice.has_power():
            self.log.info(f"Power returned after {self.statem.get_strduration()}")
            # change the state:
            self.statem.pwrBack()
            # reset battery alert status:
            self.bat.reset()
            # reset the LED light to green as we have had a power cycle
            setLedColor(0, 1, 0) # green

        # state change if both sensors are currently in the OPEN state (i.e. triggered):
        if self.wap.allOpen() and not self.statem.is_waterDetected:
            self.log.critical(f"Water detected. Previous state: {self.statem.current_state.name} ({self.statem.get_strduration()})")
            # change state machine to waterDetected
            self.statem.waterLeak()

        # -> detectors warmed up?
        warmup_duration = self.config.getint('hv', 'warmup_duration')
        if (
                self.statem.is_detectorsCold
                and self.statem.hv.is_on
                and self.statem.hv.get_duration()>datetime.timedelta(minutes=warmup_duration)):
            self.log.info(f"Detectors warmed up after {self.statem.get_strduration()}")
            # change the state to idle:
            self.statem.warmedUp()

        # idle and scheduled time has arrived?
        if (
                self.statem.is_idle
                and datetime.datetime.now() > self.scheduled_daq
                and self.pijuice.has_power()):
            self.log.info(f"Ready to take data!")
            # change state to readingData
            self.statem.startDaq()

    def update_display(self):
        """ updates display with current sensor data and status info. """
        # skip this step should the display not be active
        if self.config.getboolean('general', 'disable_display'):
            return
        self.dis.refresh(
            state=self.statem.current_state.name,
            prevpwrlss=self.bat.last_alert,
            lrun=self.last_daq,
            nrun=self.scheduled_daq
        )
        self.dis.addSensorInfo(self.sens)
        self.dis.draw()

    def run_state_action(self):
        """ executes actions related to the current state of the state machine. """
        # check for water detection and alert
        if self.wat.seen_alert():
            self.log.critical(f"Water sensor '{self.wat.where}' triggered {self.wat.when}. Current state: {self.statem.current_state.name} ({self.statem.get_strduration()})")
            # reset the alert
            self.wat.reset()

        # water detected: switch off HV
        if self.statem.is_waterDetected and self.statem.hv.is_on:
            try:
                self.log.warning("Disabling HV as water has been detected")
                hv.disableHV(self.hvcfg)
                self.statem.hv.disable()
            except hv.CmdError as e:
                self.log.error(f"{type(e).__name__}: {e}")
                self.log.info(traceback.format_exc())
            except serial.serialutil.SerialException as e:
                self.log.error(f"{type(e).__name__}: {e}")
                self.log.info(traceback.format_exc())

        # water detected: set LED to red
        if self.statem.is_waterDetected:
            setLedColor(1, 0, 0)

        # we have seen a daq fault:
        if self.statem.is_readoutFaulty:
            # power cycle PSU (12V lines to USB hub and oscilloscope):
            self.psu_relay.sethigh() # disable PSU 12V line
            time.sleep(30) # wait for things to calm down
            self.log.error("Triggering system restart to work around DAQ failure")
            # now make system call to `reboot`:
            subprocess.check_call(["sudo", "reboot"])

        # detectors cold but HV still off while we are on mains? enable HV:
        if self.statem.is_detectorsCold and self.statem.hv.is_off and self.pijuice.has_power():
            try:
                self.log.info("Enabling HV to initiate warm-up")
                hv.setValues(self.hvcfg)
                self.statem.hv.enable()
            except hv.CmdError as e:
                self.log.error(f"{type(e).__name__}: {e}")
                self.log.info(traceback.format_exc())
            except serial.serialutil.SerialException as e:
                self.log.error(f"{type(e).__name__}: {e}")
                self.log.info(traceback.format_exc())


        # ready to start the data acquisition
        if self.statem.is_readingData:
            # get list of channels that we shall record data from
            osccfg = config['oscilloscope']
            actch = [int(ch) for ch in osccfg.get('active_channels').split()]
            self.log.info("Going to record data from channels "+', '.join(map(str, actch)))
            self.last_daq = datetime.datetime.now()
            # where to store data and time string to use in file names
            timestr = time.strftime("%Y%m%d-%H%M%S")
            path = self.config.get('general', 'log_path')
            # list of booleans to track data save progress
            data_saved = []
            # add (and remove) log handler to write to separate log file for this run
            runhandler = logsetup.get_handler(
                os.path.join(path, f"run_{self.runno:06d}_{timestr}.log"),
                logsetup.frmt_full,
                "%d.%m. %H:%M:%S",
                logging.INFO)
            self.log.addHandler(runhandler)
            # loop over channels
            for ch in actch:
                # loop over all oscilloscope config files specified by the user
                # for all channels:
                osc_configs = osccfg.get('config_files', fallback = "").split()
                # specific for this channel:
                osc_configs += osccfg.get(f'config_files_ch{ch}', fallback = "").split()
                # prefix each config file with the path element
                osc_configs = [os.path.join(self.config.get('general', 'config_path'), c) for c in osc_configs]
                self.log.info(f"Have {len(osc_configs)} data acquisitions for channel #{ch}")
                for osccfgno, osccfgfil in enumerate(osc_configs):
                    # feed the watchdog as this can take a while for each channel
                    feed_watchdog()
                    if self.bat.seen_alert():
                        self.log.info(f"Battery alert! Skipping acquisition for ch #{ch}, cfg #{osccfgno}")
                        continue
                    if self.statem.is_readoutFaulty:
                        self.log.info(f"Fault in data readout! Skipping acquisition for ch #{ch}, cfg #{osccfgno}")
                        continue
                    if not os.path.isfile(osccfgfil):
                        self.log.error(f"Missing cfg file: {osccfgfil}")
                        setLedColor(0, 0, 1) # blue
                        continue
                    self.log.info(f"Preparing ch #{ch}, config {osccfgfil}")
                    data = self.acquire_data(ch, osccfgfil)
                    if self.statem.is_readoutFaulty:
                        self.log.info(f"Fault in data readout! Skipping data saving for ch #{ch}, cfg #{osccfgno}")
                        continue
                    if not data is None:
                        # construct file name
                        fn = f"run_{self.runno:06d}_ch{ch}_cfg{osccfgno}_{timestr}.bin"
                        # first write data to HDD
                        data_saved.append(self.save_to_drive(data,
                                                             config.get('general', 'hdd_path'),
                                                             fn,
                                                             osccfgfil,
                                                             "hdd"))
                        # now write a copy of the data to the thumb drive
                        data_saved.append(self.save_to_drive(data,
                                                             config.get('general', 'key_path'),
                                                             fn,
                                                             osccfgfil,
                                                             "key"))
                        # change state machine to idle again (if no fault occured)
                        if self.statem.is_savingData:
                            self.statem.dataSaved()
                    else:
                        self.log.error("No data was recorded!")
                        self.statem.readoutFault()
            # remove this run's log handler again
            self.log.removeHandler(runhandler)
            # set status LED to blue if fault detected
            if self.statem.is_readoutFaulty:
                setLedColor(0, 0, 1) # blue
            # increment and write run number to file
            self.increment_runno()
            # if nothing happened to the power, schedule next DAQ run
            if not self.bat.seen_alert() and not self.statem.is_readoutFaulty:
                if not self.daq_active['continuous']:
                    # schedule after the given wait time and within the active window:
                    self.scheduled_daq = nextRun(self.last_daq,
                                                 self.daq_active['start'],
                                                 self.daq_active['stop'])
                else:
                    # schedule as soon as possible (continuous mode):
                    self.scheduled_daq = datetime.datetime.now()
                self.log.info(f"Next run scheduled: #{self.runno} on {self.scheduled_daq}")
                # reset light to green should everything have gone smoothly
                if all(data_saved):
                    setLedColor(0, 1, 0) # green

    def acquire_data(self, ch, cfgfn):
        # change state to data reading (already set prev. for first iteration)
        if not self.statem.is_readingData:
            self.statem.startDaq()
        data = None # initialize
        try:
            # enable battery alert to raise exeception,
            # interrupting our current operation
            self.bat.raises = True
            # TODO identify and catch specific exceptions!
            data = daq.singleChRead(cfgfn, ch)
        except alerts.BatteryAlert as e:
            self.log.critical(f"Received battery operation alert while in {e.frameinfo}!")
        except daq.CommError as e:
            self.log.error(f"Exception during data taking: {e.msg}")
            self.statem.readoutFault()
        except Exception as e:
            self.log.error(f"DAQ exception {type(e).__name__}: {e}")
            self.log.info(traceback.format_exc())
            self.statem.readoutFault()
        # don't throw now -- critical path when storing data
        self.bat.raises = False
        # change state to data saving if we haven't seen a fault (in which case we break out)
        if self.statem.is_readoutFaulty:
            pass
        else:
            self.statem.dataTaken()
        return data
    
    def save_to_drive(self, data, path, fn, cfgfn, idstr):
        fnkey = os.path.join(path, fn)
        try:
            # first: make sure that the auto-mounted USB device is
            # no longer mounted at its auto-mount location:
            if idstr == 'key':
                amnt_path = "/media/usb_auto"
                if os.path.ismount(amnt_path):
                    subprocess.check_call(["sudo", "umount", amnt_path])
            # TODO if the above umount fails, one could simply adjust the path and write to the stick.
            # 
            # then: mount the device at its regular mount point (if
            # not already mounted):
            if not os.path.ismount(path):
                subprocess.check_call(["mount", path])
            # write data to file:
            with open(fnkey, 'wb') as f:
                data.tofile(f)
                self.log.info(f"Wrote data to {fnkey}")
            # copy over log files and statistics:
            pathlog = self.config.get('general', 'log_path')
            # NOTE the rsync will assume that the logs are in a separate directory.
            # If not, more than just the logs might be copied!
            subprocess.check_call(["rsync", "-a",
                                   os.path.abspath(pathlog),
                                   path])
            shutil.copy2(self.config.get('general', 'sensor_stats_file'),
                         path)
            # copy oscilloscope config file
            shutil.copy2(cfgfn, os.path.join(path, fn + '.ini'))
            # update drive usage statistics:
            total, used, free = shutil.disk_usage(path)
            self.sens.update(idstr, 100*free/total)
            # umount drive:
            subprocess.check_call(["umount", path])
            # everything worked!
            return True
        except Exception as e:
            # workaround to have separate linno for different devices in the error message
            if idstr == 'hdd':
                self.log.error(f"{idstr}: {type(e).__name__}: {e}")
            else:
                self.log.error(f"{idstr}: {type(e).__name__}: {e}")
            self.log.info(traceback.format_exc())
            setLedColor(0, 0, 1) # blue
            return False

    def increment_runno(self):
        self.runno += 1
        with open(self.runnofn, 'w') as f:
            f.write(str(self.runno))

def get_git_hash(cwd):
    return subprocess.check_output(["git", "describe", "--always"],
                                   cwd=cwd).strip().decode()

def is_repo_dirty(cwd):
    # check whether there are modified files present:
    out = subprocess.check_output(["git", "status", "--untracked-files=no", "--porcelain"],
                                  cwd=cwd)
    return True if out else False


def main(config, log):

    # set up signal handler
    sighalt = SignalHandler()
    # check git status
    cwd = os.path.dirname(os.path.realpath(__file__)) # this repo dir
    h = get_git_hash(cwd)
    log.info(f"Current CLABDAQ git revision: {h}")
    if (is_repo_dirty(cwd)):
        log.error(f"Git working tree has uncommited changes")

    # initialize application
    clabdaq = Application(config, log)
    # wait out grace period (if configured)
    clabdaq.grace_sleep(config.getint('general', 'grace_period'))

    # main /while/ loop
    # ---------
    while not sighalt.halt_now:
        # feed the watchdog
        feed_watchdog()

        # update sensor data
        # ------------------
        clabdaq.update_sensors()

        # check for state change requirements being met
        # ----------------------
        clabdaq.update_state()

        # update display
        # --------------
        clabdaq.update_display()

        # check for state activities to be executed
        #           -------------------------------
        clabdaq.run_state_action()

        # update display
        # --------------
        clabdaq.update_display()

        # wait before continuing in loop
        time.sleep(30)

if __name__ == '__main__':
    args = parse_arguments()
    # load configuration parameters from file
    # ------------------
    print(f"Loading configuration file {args.config}")
    # default config options
    cfgdefaults = {
        'log_path' : './',
        'log_level' : 'info',
        'config_path' : './config',
        'hdd_path' : "/media/hdd",
        'key_path' : "/media/usb",
        'sensor_stats_file' : "sensor_stats.csv",
        'pid_file' : "run_piautodaq.pid",
        'warmup_duration' : "30",
        'grace_period' : "10",
        'continuous_mode' : False,
        'disable_display' : False,
        'passive_mode' : False
    }
    config = configparser.ConfigParser(defaults=cfgdefaults, interpolation=configparser.ExtendedInterpolation())
    config.read(args.config)
    # initialize logging
    log = logsetup.init_logging("root",
                                path=config.get('general', 'log_path'),
                                log_level=config.get('general', 'log_level'))
    # handle SIGTERM/SIGNINT events gracefully:
    try:
        main(config=config, log=log)
    except SystemExit:
        pass
    except Exception as e:
        log.error(f"{type(e).__name__}: {e}")
        log.info(traceback.format_exc())
        sys.exit(1) # we have had a failure, exit code != 0
    finally:
        setLedColor(0, 0, 0) # off
        print('CLABDAQ finished')
