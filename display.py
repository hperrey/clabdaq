#!/usr/bin/python3

##
# Prerequisites:
# A screen properly installed on your system with a device to output to it, e.g. /dev/fb1
##

import os
import datetime
import pygame
import pygame.freetype as freetype
import subprocess
from uptime import uptime

import sensor

# General program variables from here.  Not normally modified
colors = {
    "grey_light"    :   pygame.Color(200, 200, 200),
    "grey_dark"     :   pygame.Color(100, 100, 100),
    "green"         :   pygame.Color(50, 255, 63),
    "red"           :   pygame.Color(220, 30, 30),
    "blue"          :   pygame.Color(0, 255, 255),
    "black"         :   pygame.Color(0,0,0),
    "yellow"        :   pygame.Color(240, 240, 20),
    "orange"        :   pygame.Color(255, 140, 0),
    "white"         :   pygame.Color(255, 255, 255)
}


class Display:
    """ Class to interact with an LCD display via the framebuffer and update status information. """
    # color used for font:
    tcolor = colors['green']

    def __init__(self, x = 800, y = 480, alarmfn = 'alarms.log', faultfn = 'faults.log'):
        """Arguments: `x` and `y` specify the dimension of the screen in pixels in x
        and y direction, respectively. `alarmfn` and `faultsfn` specify the log files which contain alarm and warning/error messages, respectively."""
        self.screen_x = x
        self.screen_y = y
        self.surfaceSize = (self.screen_x, self.screen_y)
        self.alarmfn = alarmfn
        self.faultfn = faultfn

        # Note that we don't instantiate any display!
        pygame.init()

        # The pygame surface we are going to draw onto.
        # /!\ It must be the exact same size of the target display /!\
        self.lcd = pygame.Surface(self.surfaceSize)

        pygame.font.init()
        #defaultFont = pygame.font.SysFont(None,30)
        # use SourceCodePro as default font (located in the same directory as this file)
        fontdir = os.path.dirname(os.path.abspath (__file__))
        fontfile = os.path.join (fontdir, "SourceCodePro-Regular.ttf")
        self.defaultFont = freetype.Font(fontfile)
        self.defaultFont.pad = True

    def __scrfrac(self, x, y):
        """ returns the pixel position on the display corresponding to the fractions `x` and `y` (in percent). """
        return (x*self.screen_x/100, y*self.screen_y/100)

    def draw(self):
        """ Writes the current surface to the frame buffer  """
        # We open the TFT screen's framebuffer as a binary file. Note that we will write bytes into it, hence the "wb" operator
        f = open("/dev/fb0","wb")
        # According to the TFT screen specs, it supports only 16bits pixels depth
        # Pygame surfaces use 24bits pixels depth by default, but the surface itself provides a very handy method to convert it.
        # once converted, we write the full byte buffer of the pygame surface into the TFT screen framebuffer like we would in a plain file:
        f.write(self.lcd.get_buffer())
        # We can then close our access to the framebuffer
        f.close()

    def screendump(self, fn = "screenshot.png"):
        """write surface to disk"""
        pygame.image.save(self.lcd, fn)

    def clear(self):
        # clear surface
        self.lcd.fill(colors['black'])

    def message(self, message, pos=[5,50], size=30):
        """ display `message` on the screen. Position is determined by x and y values in list `pos` and font size by parameter `size`. """
        # time and date
        self.defaultFont.render_to(self.lcd, self.__scrfrac(0,0), datetime.datetime.now().strftime('%d.%m. %H:%M'),
                              fgcolor=self.tcolor, bgcolor=colors["black"], size=40, style=freetype.STYLE_STRONG)
        # last/next run
        self.defaultFont.render_to(self.lcd, self.__scrfrac(pos[0], pos[1]), f"{message}",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=size, style=freetype.STYLE_STRONG)

    def refresh(self, state, prevpwrlss, lrun, nrun):
        # clear surface
        self.clear()
        # time and date
        self.defaultFont.render_to(self.lcd, self.__scrfrac(0,0), datetime.datetime.now().strftime('%d.%m. %H:%M'),
                              fgcolor=self.tcolor, bgcolor=colors["black"], size=40, style=freetype.STYLE_STRONG)

        # status
        self.defaultFont.render_to(self.lcd, self.__scrfrac(2,12), f"State: {state}",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=25, style=freetype.STYLE_STRONG)

        # power
        if not prevpwrlss is None:
            self.defaultFont.render_to(self.lcd, self.__scrfrac(5,30), f"Last power outtage: {prevpwrlss:%d.%m. %H:%M}",
                                       fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)

        # last/next run
        self.defaultFont.render_to(self.lcd, self.__scrfrac(5,35), f"Last run: {lrun:%d.%m.%y %H:%M}, next run: {nrun:%d.%m. %H:%M}",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)


        # git commit
        try:
            hash = subprocess.check_output(["git", "describe", "--always"],
                                           cwd=os.path.dirname(os.path.realpath(__file__))).strip().decode()
        except Exception as e:
            self.log.error(f"Could not determine last git commit's hash: {e}")
            hash = "None"
        self.defaultFont.render_to(self.lcd, self.__scrfrac(50,0), f"last commit {hash}",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)

        # uptime
        self.defaultFont.render_to(self.lcd, self.__scrfrac(50,4), f"uptime: {str(datetime.timedelta(seconds=uptime())).split('.')[0]}",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)


        # alarms
        num_lines = sum(1 for line in open(self.alarmfn))

        def tail(f, n):
            proc = subprocess.Popen(['tail', '-n', str(n), f], stdout=subprocess.PIPE)
            lines = proc.stdout.readlines()
            return lines
        lines = tail(self.alarmfn,3)
        self.defaultFont.render_to(self.lcd, self.__scrfrac(2,60), f"Have seen {num_lines} alarms, last three are:",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=20, style=freetype.STYLE_STRONG)
        for i,l in enumerate(lines):
            self.defaultFont.render_to(self.lcd, self.__scrfrac(2,66+4*i), l.decode().replace('\r', '').replace('\n', ''),
                       fgcolor=colors['red'], bgcolor=colors["black"], size=12, style=freetype.STYLE_STRONG)

        # errors
        num_lines = sum(1 for line in open(self.faultfn))
        lines = tail(self.faultfn,3)
        self.defaultFont.render_to(self.lcd, self.__scrfrac(2,80), f"Have seen {num_lines} errors, last three are:",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=20, style=freetype.STYLE_STRONG)
        for i,l in enumerate(lines):
            self.defaultFont.render_to(self.lcd, self.__scrfrac(2,86+4*i), l.decode().replace('\r', '').replace('\n', ''),
                       fgcolor=colors['orange'], bgcolor=colors["black"], size=12, style=freetype.STYLE_STRONG)


    def addSensorInfo(self, sens):
        """ adds sensor information from `Sensor` object `sens`. """
        def fmt(d):
            """ formats data `d` and returns string """
            if not d is None:
                return f'{d:.0f}' # format as float
            else:
                return '??'

        # disk space
        disks = sens.get_all(sensor.SensorType.diskspace)
        self.defaultFont.render_to(self.lcd, self.__scrfrac(5,20), f"Disk space:",
                                   fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)
        for i, d in enumerate(disks):
            if d['cur'] is None:
                continue
            self.defaultFont.render_to(self.lcd, self.__scrfrac(19+13*i,20), f"{d['name']} {d['cur']:.1f}%",
                                       fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)

        # power
        charge = sens.get_all(sensor.SensorType.charge)[0]
        self.defaultFont.render_to(self.lcd, self.__scrfrac(5,25), f"Battery charge: {charge['cur']:.2f}%",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)
        # temperatures
        tsens = sens.get_all(sensor.SensorType.temperature)
        self.defaultFont.render_to(self.lcd, self.__scrfrac(60,8), "temp °C (cur/24h/max)",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=20, style=freetype.STYLE_STRONG)
        for i,s in enumerate(tsens):
            if s['cur'] is None:
                self.defaultFont.render_to(self.lcd, self.__scrfrac(65,13+i*5), f"* {s['name']}: Error",
                                           fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)
            else:
                self.defaultFont.render_to(self.lcd, self.__scrfrac(65,13+i*5), f"* {s['name']}: ({fmt(s['cur'])}/{fmt(s['maxday'])}/{fmt(s['max'])})",
                                           fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)
        # humidity
        hsens = sens.get_all(sensor.SensorType.humidity)
        self.defaultFont.render_to(self.lcd, self.__scrfrac(75,40), "humidity %",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=20, style=freetype.STYLE_STRONG)
        for i,s in enumerate(hsens):
            if s['cur'] is None:
                self.defaultFont.render_to(self.lcd, self.__scrfrac(70,45+i*5), f"* {s['name']}: Error",
                                           fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)
            else:
                self.defaultFont.render_to(self.lcd, self.__scrfrac(70,45+i*5), f"* {s['name']}: ({fmt(s['cur'])}/{fmt(s['maxday'])}/{fmt(s['max'])})",
                                           fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)
        # high voltage
        vsens = sens.get_all(sensor.SensorType.voltage)
        isens = sens.get_all(sensor.SensorType.current)
        self.defaultFont.render_to(self.lcd, self.__scrfrac(35,40), "HV mon (V/I)",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=20, style=freetype.STYLE_STRONG)
        for i,s in enumerate(vsens):
            si = isens[i] # corresponding current for this channel
            xpos = [32, 50]
            cur_pos = xpos[i%2] # alternating position
            if s['cur'] is None or si['cur'] is None:
                self.defaultFont.render_to(self.lcd, self.__scrfrac(cur_pos,45+int(i/2)*5), f"{i}: Error",
                                           fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)
            else:
                self.defaultFont.render_to(self.lcd, self.__scrfrac(cur_pos,45+int(i/2)*5), f"{i}: ({fmt(s['cur'])}/{fmt(si['cur'])})",
                                           fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)
        # water
        wsens = sens.get_all(sensor.SensorType.water)
        self.defaultFont.render_to(self.lcd, self.__scrfrac(2,40), "water sensors",
                       fgcolor=self.tcolor, bgcolor=colors["black"], size=20, style=freetype.STYLE_STRONG)
        for i,s in enumerate(wsens):
            self.defaultFont.render_to(self.lcd, self.__scrfrac(5,45+i*5), f"* {s['name']}: {s['cur']}",
                                  fgcolor=self.tcolor, bgcolor=colors["black"], size=15, style=freetype.STYLE_STRONG)


def main():
    d = Display()
    d.refresh("Warming up", datetime.datetime.now(), datetime.datetime.now(), datetime.datetime.now())
    sens = sensor.main(10)
    print("Adding sensor information to display")
    d.addSensorInfo(sens)
    d.screendump()
    try:
        d.draw()
    except PermissionError as e:
        print(f"Caught exception drawing contents: {e}")
    print("Done with display tests!")

if __name__ == '__main__':
    main()
