#!/usr/bin/env python3
#
# Tools to interact with CAEN NDT1470 High-Voltage power supplies connected via
# ethernet interface or USB. Uses TCP/IP sockets or serial connection to send
# and receive commands.
#
import time
import logging
import configparser
import serial # usb
import socket # tcp/ip

class Errors(Exception):
    """Base class for exceptions in this module."""
    pass

class CmdError(Errors):
    """Exception raised when command sent to device returns with error msg.

    Attributes:
        msg -- explanation of the alert
            par -- the parameter sent to the device that caused the error
    """

    def __init__(self, msg, par):
        self.par = par
        self.msg = msg
        # Call the base class constructor with the parameters it needs
        super().__init__(msg)

class CfgError(Errors):
    """Exception raised when there is an issue with the configuration.

    Attributes:
        msg -- explanation of the alert
            par -- the parameter missing in the config
    """

    def __init__(self, msg, par):
        self.par = par
        self.msg = msg
        # Call the base class constructor with the parameters it needs
        super().__init__(msg)


class CaenStatDecode:
    """ decodes status bits (see p25 of the NDT1470 user manual (UM2027, rev.17). )"""
    bitfcn = ['ON 1 : ON 0 : OFF', 'RUP 1 : Channel Ramp UP', 'RDW 1 : Channel Ramp DOWN',
              'OVC 1 : IMON >= ISET', 'OVV 1 : VMON > VSET + 2.5 V', 'UNV 1 : VMON < VSET – 2.5 V',
              'MAXV 1 : VOUT in MAXV protection', 'TRIP 1 : Ch OFF via TRIP (Imon >= Iset during TRIP)',
              'OVP 1 : Output Power > Max', 'OVT 1: TEMP > 105°C', 'DIS 1 : Ch disabled (REMOTE Mode and Switch on OFF position)',
           'KILL 1 : Ch in KILL via front panel', 'ILK 1 : Ch in INTERLOCK via front panel', 'NOCAL 1 : Calibration Error']
    def __init__(self, n):
        self.msg = [m for i, m in enumerate(self.bitfcn) if (n & ( 1 << i))]

class CaenBoardAlarmDecode:
    """ decodes board alarm status bits (see p26 of the NDT1470 user manual (UM2027, rev.17). )"""
    bitfcn = ["Ch0 in Alarm status", "Ch1 in Alarm status", "Ch2 in Alarm status", "Ch3 in Alarm status",
              "Board in POWER FAIL", "Board in OVER POWER", "Internal HV Clock FAIL"]
    def __init__(self, n):
        self.msg = [m for i, m in enumerate(self.bitfcn) if (n & ( 1 << i))]


class CaenDecode:
    """ decodes the string sent back by the NDT1470. Based on information on p24 of the NDT1470 user manual (UM2027, rev.17). """
    def __init__(self, s):
        self.s = s
        # check if string is empty
        if s and 'CMD:OK' in s:
                self.ok = True
        else:
            self.ok = False
    def errmsg(self):
        """ decodes error message """
        if self.ok:
            return "OK"
        elif 'CMD:ERR' in self.s:
            return "Wrong command Format or command not recognized"
        elif 'CH:ERR' in self.s:
            return "Channel Field not present or wrong Channel value"
        elif 'PAR:ERR' in self.s:
            return "Field parameter not present or parameter not recognized"
        elif 'VAL:ERR' in self.s:
            return "Wrong set value (<Min or >Max)"
        elif 'LOC:ERR' in self.s:
            return "Command SET with module in LOCAL mode"
        elif not self.s:
            return "Received no response"
        else:
            return f"UNKNOWN ERROR: received '{self.s}'"
    def val(self):
        """ decodes values returned from the device """
        if not 'VAL:' in self.s:
            return None
        # get values from string:
        # indicator is 'VAL:', separator ';'
        vstr = self.s[self.s.find('VAL:')+4:]
        v = [float(num) if '.' in num else int(num) for num in vstr.split(';')]
        # return just number of lonely element
        if len(v) == 1:
            return v[0]
        # else, return whole list
        return v

class CaenNDT1470:
    """Class for interacting with a CAEN NDT1470 NIM HV module via ethernet
(telnet) interface or serial (USB) connection. """

    # available parameters (to monitor) in the NDT1470
    parameters_get = ["VSET", "VMIN", "VMAX", "VDEC", "VMON", "ISET", "IMIN", "IMAX", "ISDEC", "IMON", "IMRANGE", "IMDEC", "MAXV", "MVMIN", "MVMAX", "MVDEC", "RUP", "RUPMIN", "RUPMAX", "RUPDEC", "RDW", "RDWMIN", "RDWMAX", "RDWDEC", "TRIP", "TRIPMIN", "TRIPMAX", "TRIPDEC", "PDWN", "POL", "STAT"]
    parameters_set = ["VSET", "ISET", "MAXV", "RUP", "RDW", "TRIP", "PDWN", "IMRANGE", "ON", "OFF", "BDCLR"]
    nchannels = 4
    
    def __init__(self, address = None, device = None):
        self.log = logging.getLogger('root')
        self.address = address
        self.device = device
        self.sock = None
        self.ser = None
        if self.address:
            self.connect_tcp()
        elif self.device:
            self.connect_serial()
    def connect_tcp(self):
        """ establishes the connection via ethernet interface. """
        self.log.debug(f"Connecting via TCP to {self.server_address}")
        # Create a TCP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (self.address, 1470) # default port for NDT1470: 1470
        # set timeout
        self.sock.settimeout(1) # tcp connection will take time
        # establish connection
        self.sock.connect(server_address)
        # set to non-blocking
        #self.sock.setblocking(False)
    def connect_serial(self):
        """ connects to the device via serial connection """
        # make a default (19200,8,N,1) connection
        self.log.debug(f"Connecting via serial interface to {self.device}")
        self.ser = serial.Serial(self.device, timeout = 0)
    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_value, traceback):
        # terminate connection
        if not self.sock is None:
            self.sock.close()
        if not self.ser is None:
            self.ser.close()
    def get(self, ch, par, bd=0):
        """Get (monitor) values for module 'bd', channel 'ch' and parameter 'par'
        Available parameters to monitor are: VSET, VMIN, VMAX, VDEC, VMON, ISET, IMIN,
        IMAX, ISDEC, IMON, IMRANGE, IMDEC, MAXV, MVMIN, MVMAX, MVDEC, RUP, RUPMIN,
        RUPMAX, RUPDEC, RDW, RDWMIN, RDWMAX, RDWDEC, TRIP, TRIPMIN, TRIPMAX, TRIPDEC,
        PDWN, POL, STAT. Raises CmdError when fails. """
        res = self.send_cmd(ch=ch, par=par, bd=bd, val=None)
        if not res.ok:
            raise CmdError(par=par, msg=f"Error when monitoring {par} for ch {ch}: {res.errmsg()}")
        return res.val()

    def set(self, ch, par, val, bd=0):
        """Set values for module 'bd', channel 'ch' and parameter 'par' to value 'val'.
        Available parameters are VSET, ISET, MAXV, RUP, RDW, TRIP, PDWN, IMRANGE, ON, OFF, BDCLR.  Raises CmdError when fails. """
        res = self.send_cmd(ch=ch, par=par, bd=bd, val=val)
        # test whether we received an error back:
        # NOTE This ignores cases where we received *no* response from the device;
        # noticed no response behavior on set cmds which still resulted in the desired change being active.
        # This is considered a workaround.
        # TODO investigate further and consult CAEN
        if not res.ok and res.s:
            raise CmdError(par=par, msg=f"Error when setting {par} for ch {ch}: {res.errmsg()}")
        return res.val()

    def send_cmd(self, ch, par, bd=0, val=None):
        """ constructs and sends cmd to device. Wraps result into a CaenDecode object. """
        cmd = f'$BD:{bd:02},'
        par = par.upper()
        # if we don't have a channel then this is a module cmd
        chstr = f'CH:{ch:02},' if not ch is None else ""
        if val is None:
            if not par=='ON' and not par=='OFF':
                # monitoring
                cmd += f'CMD:MON,{chstr}PAR:{par}'
            else:
                # switching on/off
                cmd += f'CMD:SET,{chstr}PAR:{par}'
        else:
            # setting value
            cmd += f'CMD:SET,{chstr}PAR:{par},VAL:{val}'
        self.log.debug(f"Sending cmd: {cmd}")
        self.send_raw(cmd)
        return CaenDecode(self.receive_raw())

    def send_raw(self, msg):
        """ sends a (raw) command to the device. `msg' is a string that will be byte-encoded before sending. From the NDT1470 user manual (UM2027, rev.17, p24) :

           The Format of a command string is the following :
           $BD:**,CMD:***,CH*,PAR:***,VAL:***.**<CR, LF >
           The fields that form the command are :
           BD : 0..31 module address (to send the command)
           CMD : MON, SET
           CH : 0..NUMCH (NUMCH=2 for N1570, NUMCH=4 for the other modules)
           PAR : (see parameters tables)
           VAL : (numerical value must have a Format compatible with resolution and range)

        """
        # appends terminating characters (carriage return and line feed)
        msg = f'{msg}\r\n'.encode()
        if not self.sock is None:
            self.sock.sendall(msg)
        elif not self.ser is None:
            self.ser.write(msg)
        else:
            self.log.error("No device connected!")
        time.sleep(0.1) # best to wait before continuing..

    def receive_raw(self):
        """ receives (raw) response from last issued command to the device. """
        # Wait for events...
        time.sleep(0.2) # best to wait before continuing..
        buffer = ''
        # TCP/IP
        if not self.sock is None:
            try:
                buffer = self.sock.recv(1024).decode()
            except socket.error as e:
                #if e.errno != errno.EWOULDBLOCK:
                self.log.error('Error: %r' % e)
                # If e.errno is errno.EWOULDBLOCK, then no more data
            #data = self.sock.recv(1024)
        # serial
        elif not self.ser is None:
            buffer = self.ser.read(1024).decode()
        self.log.debug(f"Received response: {buffer}")
        return buffer

def loadConfig(filename):
    defaults = {
        'channel defaults' : {
            'enabled': False,
            'vset' : 0,
            'iset': 0}}
    cfg = configparser.ConfigParser(default_section='channel defaults')
    cfg.read_dict(defaults)
    cfg.read(filename)
    return cfg

def getConnectionParams(cfg):
    try:
        devcfg = cfg['device']
    except KeyError:
        raise CfgError("Missing 'device' section in config file","device")
    try:
        if devcfg['connection_type'] == "serial":
            # connect via serial interface
            address = None
            device = devcfg['connect']
        else:
            # connect via ethernet
            address = devcfg['connect']
            device = None
    except KeyError:
        raise CfgError("Missing 'connection_type' and matching 'address' or 'device' parameter in config file","connection_type")
    return address, device

def disableHV(cfg):
    with CaenNDT1470(*getConnectionParams(cfg)) as hv:
        # loop over channels
        for ch in range(hv.nchannels):
            # disable channel
            hv.set(ch=ch, par='off', val=None)

def setValues(cfg):
    with CaenNDT1470(*getConnectionParams(cfg)) as hv:
        # loop over channels
        for ch in range(hv.nchannels):
            if cfg.getboolean(f'channel {ch}', 'enabled'):
                # enable channel
                hv.set(ch=ch, par='on', val=None)
            else:
                # disable channel
                hv.set(ch=ch, par='off', val=None)
            # optional values:
            try:
                # set max voltage
                hv.set(ch=ch, par='maxv',
                       val=cfg.getint(f'channel {ch}', 'maxv'))
            except configparser.NoOptionError:
                pass
            try:
                # set trip time
                hv.set(ch=ch, par='trip',
                       val=cfg.getint(f'channel {ch}', 'trip'))
            except configparser.NoOptionError:
                pass
            try:
                # set ramp up
                hv.set(ch=ch, par='rup',
                       val=cfg.getint(f'channel {ch}', 'rup'))
            except configparser.NoOptionError:
                pass
            try:
                # set ramp down
                hv.set(ch=ch, par='rdw',
                       val=cfg.getint(f'channel {ch}', 'rdw'))
            except configparser.NoOptionError:
                pass
            try:
                # set power down mode (ramp/kill)
                hv.set(ch=ch, par='pdwn',
                       val=cfg.get(f'channel {ch}', 'pdwn'))
            except configparser.NoOptionError:
                pass         
            # set voltage
            hv.set(ch=ch, par='vset',
                   val=cfg.getint(f'channel {ch}', 'vset'))
            # set current limit
            hv.set(ch=ch, par='iset',
                   val=cfg.getint(f'channel {ch}', 'iset'))

def getValues(cfg):
    res = {}
    with CaenNDT1470(*getConnectionParams(cfg)) as hv:
        pars = ['VSET', 'VMON', 'ISET', 'IMON', 'STAT']
        # loop over channels
        for ch in range(hv.nchannels):
            chres = {}
            for p in pars:
                v = hv.get(ch=ch, par=p)
                if p == 'STAT':
                    # decode status messages
                    chres['STAT_MSG'] = CaenStatDecode(v).msg
                # append value to dict
                chres[p] = v
            # append to dict
            res[f'channel {ch}'] = chres
        # board alarm status bits
        v = hv.get(ch=None, par='BDALARM')
        res['BDALARM'] = CaenBoardAlarmDecode(v).msg
    return res

def printValues(val):
    """ pretty-prints a table with status data from HV module. """
    # unpack results into rows and colum headers
    rows = [r for r in val.keys()]
    cols = [c for c in val[rows[0]].keys()]
    print("\t\t\t"+"\t".join(cols))
    for r in rows:
        print(f"\n{r}", end='\t')
        try:
            for k, v in val[r].items():
                print(v, end="\t")
        except AttributeError:
            print(val[r])

def test():
    """ demo/testing routine """
    logging.basicConfig(level="DEBUG")
    log = logging.getLogger('root')
    cfg = loadConfig("./config/hv.ini")
    log.info("Retrieving values:")
    print(getValues(cfg))
    time.sleep(4)
    log.info("Disabling HV:")
    disableHV(cfg)
    time.sleep(4)
    log.info("Retrieving values:")
    print(getValues(cfg))
    time.sleep(4)
    log.info("Setting values according to cfg file:")
    setValues(cfg)
    time.sleep(4)
    log.info("Retrieving values according to cfg file:")
    print(getValues(cfg))

def main():
    """ Sets values according to 'hv.ini' config file. """
    logging.basicConfig(level="DEBUG")
    log = logging.getLogger('root')
    cfg = loadConfig("./config/hv.ini")
    log.info("Setting values according to cfg file:")
    setValues(cfg)
    time.sleep(2)
    log.info("Retrieving values:")
    printValues(getValues(cfg))

if __name__ == '__main__':
    main()
