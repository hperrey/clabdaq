#!/usr/bin/env python3
import socket
import time
import argparse

def setLedColor(r, g, b):
    """connects to the locally-running pilamp server and requests a LED color
    change to the r, g, b (fractional) values specified as parameters."""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', 12345) # default port for pilamp server
    # set timeout
    sock.settimeout(1) # tcp connection will take time
    # establish connection
    try:
        sock.connect(server_address)
        time.sleep(0.1)
    except Exception as e:
        print('Error communicating with pilamp server: %r' %e)
        return
    try:
        buffer = sock.recv(1024).decode()
        #print(f'Setting LED color to {r} {g} {b}')
        msg = f'set_rgb {r} {g} {b}'
        sock.sendall(msg.encode())
        time.sleep(0.1)
        buffer = sock.recv(1024).decode()
    except socket.error as e:
        print('Error communicating with pilamp server: %r' % e)
        return
    if buffer.startswith('OK'):
        pass
        #print(f"Got response from pilamp server: 'OK'")
    # close connection
    sock.close()

def parse_arguments():
    parser = argparse.ArgumentParser(description='Set LED color of the LED lamp server running on localhost.')
    parser.add_argument('red', type=float)
    parser.add_argument('green', type=float)
    parser.add_argument('blue', type=float)
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()
    setLedColor(args.red, args.green, args.blue)
