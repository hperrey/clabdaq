#!/usr/bin/env python3
#
# This module provides classes representing a state machine for the DAQ system.
#

from statemachine import StateMachine, State
import logging
import datetime


class HvPowerMachine(StateMachine):
    """ A state machine for HV power """
    off = State("off", initial=True)
    on  = State("on")

    enable = off.to(on)
    disable = on.to(off)

    def __init__(self, **kwargs):
        self.timestamp = datetime.datetime.now()
        super().__init__(kwargs)
    def update_state_change(self):
        self.timestamp = datetime.datetime.now()
    def get_duration(self):
        """ returns the duration of the current state as timedelta object """
        return datetime.datetime.now()-self.timestamp
    def on_enter_on(self):
        self.update_state_change()
    def on_enter_off(self):
        self.update_state_change()

class AutoDaqMachine(StateMachine):
    """An autononomous data acquisition status machine"""
    uninitialized = State('uninitialized', initial=True)
    passive       = State('passive')
    detectorsCold = State('detectorsCold')
    idle          = State('idle')
    readingData   = State('readingData')
    savingData    = State('savingData')
    readoutFaulty = State('readoutFaulty')
    batteryPwd    = State('batteryPwd')
    waterDetected = State('waterDetected')

    configured = uninitialized.to(detectorsCold)
    passiveModeEnabled = uninitialized.to(passive)
    warmedUp = detectorsCold.to(idle)
    pwrLoss = idle.to(batteryPwd) | detectorsCold.to(batteryPwd) | readingData.to(batteryPwd) | passive.to(passive)
    startDaq = idle.to(readingData)
    dataTaken = readingData.to(savingData)
    dataSaved = savingData.to(idle)
    readoutFault = readingData.to(readoutFaulty)
    waterLeak = idle.to(waterDetected) | detectorsCold.to(waterDetected) | readingData.to(waterDetected) | batteryPwd.to(waterDetected) | passive.to(waterDetected) | readoutFaulty.to(waterDetected)
    pwrBack = batteryPwd.to(detectorsCold)

    def __init__(self, **kwargs):
        self.timeEntered = datetime.datetime.now()
        self.log = logging.getLogger('root')
        self.hv = HvPowerMachine()
        super().__init__(kwargs)

    def get_duration(self):
        """ returns the duration of the current state as timedelta object """
        return datetime.datetime.now()-self.timeEntered

    def get_strduration(self):
        """ returns duration as a string with milliseconds removed. """
        return str(self.get_duration()).split('.')[0] # convert to str, remove milliseconds

    def update_state_change(self):
        self.log.info(f"Entered state {self.current_state.name} after {self.get_strduration()}")
        self.timeEntered = datetime.datetime.now()

    def on_enter_uninitialized(self):
        self.update_state_change()
    def on_enter_passive(self):
        self.update_state_change()
    def on_enter_detectorsCold(self):
        self.update_state_change()
    def on_enter_idle(self):
        self.update_state_change()
    def on_enter_readingData(self):
        self.update_state_change()
    def on_enter_savingData(self):
        self.update_state_change()
    def on_enter_batteryPwd(self):
        self.update_state_change()
        if self.hv.is_on:
            self.hv.disable() # no mains, no power to HV anymore
    def on_enter_waterDetected(self):
        self.update_state_change()


def main():
    hv = HvPowerMachine()
    print(f"HV on? {hv.is_on}")
    print("enable..")
    hv.enable()
    print(f"HV on? {hv.is_on}")
    print("wait..")
    import time
    time.sleep(2)
    print(f"HV is on for {hv.get_duration()}")
    

if __name__ == '__main__':
    main()
