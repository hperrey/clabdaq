#
# logging setup routines
#

import logging
from logging.handlers import TimedRotatingFileHandler
import os

frmt_full = '%(asctime)s %(levelname)s [%(filename)s:%(lineno)d]: %(message)s'
frmt_short = '%(asctime)s [%(filename)s:%(lineno)d]: %(message)s'

def get_handler(fn, frmt, frmt_time, level):
    formatter = logging.Formatter(frmt, frmt_time)
    handler = logging.FileHandler(fn)
    handler.setFormatter(formatter)
    handler.setLevel(level)
    return handler

def init_logging(logger, path='./', log_level = None):
    """ initializes logging """
    log = logging.getLogger(logger)  # get reference to logger
    # test if we have set up the logger before
    if not len(log.handlers):
        # perform setup by adding handlers starting with stream (console):
        formatter = logging.Formatter(frmt_full,"%d.%m. %H:%M:%S")
        handler_stream = logging.StreamHandler()
        handler_stream.setFormatter(formatter)
        log.addHandler(handler_stream)
        # add TIME ROTATING file handler to write all log entries to file and
        # rotate them each monday ('w0'). BackupCount=0 disables the removal of
        # old logs.
        logfn = os.path.join(path, "messages.log")
        handler_filall = TimedRotatingFileHandler(logfn, when='W0', backupCount=0)
        handler_filall.setFormatter(formatter)
        handler_filall.setLevel(logging.INFO)
        log.addHandler(handler_filall)
        # add a second handler for only warnings (or above); no rotation
        log.addHandler(get_handler(
            os.path.join(path, "faults.log"),
            frmt_short,
            "%d.%m. %H:%M",
            logging.WARNING))
        # add a third handler for only critical messanges (or above); no rotation
        log.addHandler(get_handler(
            os.path.join(path, "alarms.log"),
            frmt_short,
            "%d.%m. %H:%M",
            logging.CRITICAL))
        if not log_level:
            log_level = "DEBUG"
    if log_level:
        # set the logging level
        numeric_level = getattr(logging, log_level.upper(),
                                None)  # default: INFO messages and above
        log.setLevel(numeric_level)
    return log
